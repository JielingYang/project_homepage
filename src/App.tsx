// import reactLogo from './assets/react.svg'
import {CSSProperties, useEffect} from "react";
import InteractiveTilesContainer from "./interactiveTilesBackground/InteractiveTilesContainer";
import ContentsContainer from "./contents/ContentsContainer";
import {useDispatch} from "react-redux";
import {EVENT_TIME_GAP} from "./UI_CONST.ts";
import {throttle} from "lodash";
import {setMouseMovePositionOnWindow} from "./redux/reducers/mouseStateReducer.ts";

const appCss: CSSProperties = {
    position: "absolute",
    left: 0,
    top: 0,
    width: "100vw",
    height: "100vh",
};

function App() {
    const dispatch = useDispatch();

    useEffect(() => {
        const mouseMoveHandler =
          throttle((e: MouseEvent) => {
              dispatch(setMouseMovePositionOnWindow({x: e.x, y: e.y}))
          }, EVENT_TIME_GAP)

        window.addEventListener('mousemove', mouseMoveHandler);
        return () => {
            window.removeEventListener('mousemove', mouseMoveHandler);
        };
    }, [dispatch]);

    console.log('app')
    return (
        <div id={'app'} style={appCss}>
            <InteractiveTilesContainer/>
            <ContentsContainer/>
        </div>
    )
}

export default App
