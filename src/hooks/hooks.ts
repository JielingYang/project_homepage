import { ForwardedRef, RefObject, useEffect, useMemo, useState } from "react";
import { INTERSECT_ENTRY_THRESHOLDS } from "../contents/contentsUtils";
import { Nullable } from "@babylonjs/core";

export function useIntersectEntry(ref: RefObject<HTMLDivElement>) {
    const [entry, updateEntry] = useState<Nullable<IntersectionObserverEntry>>(null)

    const observer = useMemo(() => {
        return new IntersectionObserver(
            ([entry]) => {
                updateEntry(entry)
            },
            { threshold: INTERSECT_ENTRY_THRESHOLDS }
        )
    }, [])

    useEffect(() => {
        if (ref.current) observer.observe(ref.current)
        return () => {
            observer.disconnect()
        }
    }, [ref, observer])

    return entry
}

export function useIntersectRatio(ref: ForwardedRef<HTMLDivElement>) {
    const [intersectRatio, updateIntersectRatio] = useState<number>(0)

    const observer = useMemo(() => {
        return new IntersectionObserver(
            ([entry]) => {
                updateIntersectRatio(entry.intersectionRatio)
            },
            { threshold: INTERSECT_ENTRY_THRESHOLDS }
        )
    }, [])

    useEffect(() => {
        const tempRef = ref as RefObject<HTMLDivElement>
        if (tempRef.current) observer.observe(tempRef.current)
        return () => {
            observer.disconnect()
        }
    }, [ref, observer])

    return intersectRatio
}
