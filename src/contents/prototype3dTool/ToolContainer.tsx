import '@babylonjs/core/Debug/debugLayer'
import '@babylonjs/inspector'
import { CSSProperties, useCallback, useRef, useState } from 'react'
import { useIntersectRatio } from '../../hooks/hooks'
import { Nullable, Scene } from '@babylonjs/core'
import SceneComponent from 'babylonjs-hook'
import { GRAY60_100 } from '../../BABYLON_CONST.ts'
import { setupMainCamera } from './helper/camera.ts'
import { setupMaterials } from './helper/material.ts'
import { setupGroundFlat } from './helper/ground.ts'
import { setupDirectionalLight, setupHemisphericLight, setupPointLightForShadow } from './helper/light.ts'
import { setupGlowLayer } from './helper/glowLayer.ts'
import { setupAxes } from './helper/axes.ts'
import { throttle } from 'lodash'
import { pointerMoveHandler } from './helper/pointerHandler.ts'
import { EVENT_TIME_GAP } from '../../UI_CONST.ts'
import { onRender } from './helper/onRender.ts'
import { Button, SxProps } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { setIsPlacingObject } from '../../redux/reducers/generalStateReducer'
import { TRootState } from '../../redux/store'

const rootCss: CSSProperties = {
    position: 'relative',
    boxSizing: 'border-box',
    width: '100%',
    minHeight: '100vh',
    display: 'flex',
    backgroundColor: 'rgba(255,0,0,0.1)',
}

const canvasCss: CSSProperties = {
    outline: 'none',
    width: '70%',
    height: '70%',
}

const testButtonSx: SxProps = {}

const ToolContainer = () => {
    const dispatch = useDispatch()
    const ref = useRef<HTMLDivElement>(null)
    const panelIntersectRatio = useIntersectRatio(ref)
    const [, setBabylonScene] = useState<Nullable<Scene>>(null)
    const isPlacingObject = useSelector((state: TRootState) => state.generalState.isPlacingObject)

    const onSceneReady = useCallback((scene: Scene) => {
        scene.debugLayer
            .show({
                embedMode: true,
                overlay: true,
            })
            .then()

        scene.clearColor = GRAY60_100

        scene.activeCameras = [setupMainCamera(scene)]

        setupGlowLayer(scene)
        setupMaterials(scene)
        const flatGround = setupGroundFlat(scene)
        setupPointLightForShadow(scene)
        setupDirectionalLight(scene)
        setupHemisphericLight(scene)

        setupAxes(scene, 20, flatGround)

        scene.onPointerMove = throttle((event, pickInfo) => pointerMoveHandler(scene, event, pickInfo), EVENT_TIME_GAP)

        setBabylonScene(scene)
    }, [])

    console.log('prototype3dTool', panelIntersectRatio)
    return (
        <div id={'prototype3dTool'} ref={ref} style={rootCss}>
            <SceneComponent antialias onSceneReady={onSceneReady} onRender={onRender} style={canvasCss} />
            <Button onClick={() => dispatch(setIsPlacingObject(!isPlacingObject))} sx={testButtonSx}>
                {'test'}
            </Button>
        </div>
    )
}

export default ToolContainer
