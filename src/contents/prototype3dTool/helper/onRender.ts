import {LinesMesh, Mesh, Scene} from '@babylonjs/core'
import {store} from '../../../redux/store.ts'
import {MeshBuilder} from '@babylonjs/core/Meshes/meshBuilder'
import {getLocalBottom4PointsOfBoundingBox, localToWorld, point3ToVector3, worldToLocal} from '../../../UTIL.ts'
import {Vector3} from '@babylonjs/core/Maths/math.vector'
import {COORD_1, OBJECT_PLACEMENT_GHOST} from '../../../NAME.ts'
import {ObjectTags} from './tag.ts'

export const onRender = (scene: Scene) => {
    handlePlacingObject(scene)
}

const handlePlacingObject = (scene: Scene) => {
    if (store.getState().generalState.isPlacingObject) {
        const temp = store.getState().mouseState.mousePositionOnObject
        if (temp) {
            const hoveredTargets = scene.getMeshesByTags(`${ObjectTags.HOVERED_TAG} && ${ObjectTags.GRID_TAG}`)
            const hoveredTarget = hoveredTargets.length > 0 ? hoveredTargets[0] : null
            if (hoveredTarget) {
                let objectPlacementGhost = scene.getMeshByName(OBJECT_PLACEMENT_GHOST)
                if (objectPlacementGhost == null) {
                    objectPlacementGhost = MeshBuilder.CreateBox(OBJECT_PLACEMENT_GHOST)
                    objectPlacementGhost.parent = hoveredTarget
                    objectPlacementGhost.showBoundingBox = true
                    objectPlacementGhost.isPickable = false
                }
                objectPlacementGhost.position = worldToLocal(point3ToVector3(temp), hoveredTarget).addInPlace(
                    new Vector3(0, objectPlacementGhost.getRawBoundingInfo().boundingBox.extendSize.y, 0)
                )

                const pointsInLocalSpace = getLocalBottom4PointsOfBoundingBox(objectPlacementGhost.getRawBoundingInfo().boundingBox)
                const pointsInWorldSpace = pointsInLocalSpace.map((p) => localToWorld(p, objectPlacementGhost))

                // Convert min vector3 from ghost object local space to world space
                const vector3InWorldSpace = localToWorld(pointsInLocalSpace[0], objectPlacementGhost)

                updateCoordinateLinesForPoint(scene, vector3InWorldSpace, hoveredTarget)

                return
            }
        }
    }

    const objectPlacementGhost = scene.getMeshByName(OBJECT_PLACEMENT_GHOST)
    objectPlacementGhost && objectPlacementGhost.dispose()
}

const updateCoordinateLinesForPoint = (scene: Scene, point: Vector3, localTarget: Mesh) => {
    const offset = localTarget.getRawBoundingInfo().minimum

    const localMousePoint = worldToLocal(point, localTarget)

    const xPoint = localMousePoint.clone().multiply(Vector3.Right()).add(new Vector3(0, offset.y, offset.z))
    const zPoint = localMousePoint.clone().multiply(Vector3.Forward()).add(new Vector3(offset.x, offset.y, 0))

    const coord1Lines = scene.getMeshByName(COORD_1) as LinesMesh
    const points = [zPoint, localMousePoint, xPoint]
    const options = {points: points, updatable: true}

    let lines
    if (coord1Lines) {
        lines = MeshBuilder.CreateLines(COORD_1, {...options, instance: coord1Lines})
        lines.parent = localTarget
    } else {
        lines = MeshBuilder.CreateLines(COORD_1, options, scene)
        lines.parent = localTarget
    }
}
