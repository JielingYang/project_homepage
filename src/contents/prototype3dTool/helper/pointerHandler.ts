import { IPointerEvent, PickingInfo, Scene, Tags } from "@babylonjs/core";
import { store } from "../../../redux/store.ts";
import { setMousePositionOnObject } from "../../../redux/reducers/mouseStateReducer.ts";
import { ObjectTags } from "./tag.ts";

export const pointerMoveHandler = (
  scene: Scene,
  evt: IPointerEvent,
  pickInfo: PickingInfo
) => {
  updateMousePositionOnGround(scene, pickInfo);
};

const updateMousePositionOnGround = (scene: Scene, pickInfo: PickingInfo) => {
  if (
    pickInfo.pickedMesh &&
    Tags.MatchesQuery(pickInfo.pickedMesh, ObjectTags.GRID_TAG) &&
    pickInfo.pickedPoint
  ) {
    store.dispatch(
      setMousePositionOnObject({
        x: pickInfo.pickedPoint.x,
        y: pickInfo.pickedPoint.y,
        z: pickInfo.pickedPoint.z
      })
    );
  }
};
