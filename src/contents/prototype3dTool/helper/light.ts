import { AbstractMesh, DirectionalLight, HemisphericLight, MirrorTexture, PointLight, Scene, ShadowGenerator, StandardMaterial, Vector3 } from "@babylonjs/core";
import { Color3 } from "@babylonjs/core/Maths/math.color";
import { DIRECTIONAL_LIGHT01_NAME, GENERAL_SHADOW_LIGHT_NAME, GROUND_MESH_MATERIAL_NAME, HEMISPHERIC_LIGHT_NAME } from "../../../NAME.ts";
import { DIRECTIONAL_LIGHT01_VECTOR3, GRAY10, GRAY20, GRAY50, SHADOW_LIGHT_POSITION } from "../../../BABYLON_CONST.ts";

export const setupPointLightForShadow = (scene: Scene) => {
  const shadowLight = new PointLight(
    GENERAL_SHADOW_LIGHT_NAME,
    SHADOW_LIGHT_POSITION,
    scene
  );
  shadowLight.specular = GRAY50;
  shadowLight.intensity = 0.4;
  const shadowGenerator = new ShadowGenerator(2048, shadowLight);
  shadowGenerator.useBlurExponentialShadowMap = true;
  shadowGenerator.useKernelBlur = true;
  shadowGenerator.transparencyShadow = true;

  return shadowGenerator;
};

export const setupDirectionalLight = (scene: Scene) => {
  const directionalLight01 = new DirectionalLight(
    DIRECTIONAL_LIGHT01_NAME,
    DIRECTIONAL_LIGHT01_VECTOR3,
    scene
  );
  // let directionalLight02 = new DirectionalLight(
  //   DIRECTIONAL_LIGHT02_NAME,
  //   DIRECTIONAL_LIGHT02_VECTOR3,
  //   scene
  // );
  directionalLight01.specular = GRAY50;
  // directionalLight02.specular = GRAY50;
  directionalLight01.intensity = 0.5;
  // directionalLight02.intensity = 0.9;
};

export const setupHemisphericLight = (scene: Scene) => {
  const hemisphericLight = new HemisphericLight(
    HEMISPHERIC_LIGHT_NAME,
    Vector3.Up(),
    scene
  );
  hemisphericLight.intensity = 2;
  hemisphericLight.diffuse = Color3.White();
  hemisphericLight.specular = GRAY20;
  hemisphericLight.groundColor = GRAY10;
};

export const addShadowToMesh = (scene: Scene, mesh: AbstractMesh) => {
  const shadowGenerator = scene
    .getLightByName(GENERAL_SHADOW_LIGHT_NAME)
    ?.getShadowGenerator();

  if (shadowGenerator) {
    (shadowGenerator as ShadowGenerator).addShadowCaster(mesh, false);
  }
};

export const addReflectionToMesh = (scene: Scene, mesh: AbstractMesh) => {
  const material = scene.getMaterialByName(GROUND_MESH_MATERIAL_NAME);

  if (material) {
    const mirrorTexture = (material as StandardMaterial).reflectionTexture;
    if (mirrorTexture) {
      (mirrorTexture as MirrorTexture).renderList?.push(mesh);
    }
  }
};

export const addVisualEffectToMesh = (scene: Scene, mesh: AbstractMesh) => {
  addShadowToMesh(scene, mesh);
  addReflectionToMesh(scene, mesh);
};
