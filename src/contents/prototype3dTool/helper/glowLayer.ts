import { GlowLayer, Scene } from "@babylonjs/core";
import { GLOW_LAYER } from "../../../NAME.ts";

export const setupGlowLayer = (scene: Scene) => {
  const gl = new GlowLayer(GLOW_LAYER, scene);
  gl.intensity = 0.4;

  return gl;
};
