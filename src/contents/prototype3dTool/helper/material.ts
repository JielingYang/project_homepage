//@flow
import { MirrorTexture, Plane, Scene, StandardMaterial } from "@babylonjs/core";
import { GROUND_MESH_MATERIAL_NAME, GROUND_MESH_MIRROR_TEXTURE_NAME, OBSTACLE_PREVIEW_BASE_MATERIAL_NAME, WORLD_AXIS_MATERIAL_NAME } from "../../../NAME.ts";
import { GRAY10, GRAY30, OBSTACLE_PREVIEW_BASE_MATERIAL_ALPHA, PRIMARY_YELLOW } from "../../../BABYLON_CONST.ts";

export const setupMaterials = (scene: Scene) => {
    /*
    Ground material
     */
    const groundMaterial = new StandardMaterial(GROUND_MESH_MATERIAL_NAME, scene);
    groundMaterial.diffuseColor = GRAY10;
    groundMaterial.specularColor = GRAY30;
    const mirrorTexture = new MirrorTexture(
        GROUND_MESH_MIRROR_TEXTURE_NAME,
        {ratio: 0.4},
        scene,
        true
    );
    mirrorTexture.mirrorPlane = new Plane(0, -1, 0, 0);
    mirrorTexture.level = 0.7;
    mirrorTexture.adaptiveBlurKernel = 100;
    groundMaterial.reflectionTexture = mirrorTexture;

    /*
    World axis material
     */
    const worldAxisMaterial = new StandardMaterial(WORLD_AXIS_MATERIAL_NAME, scene);
    worldAxisMaterial.emissiveColor = PRIMARY_YELLOW;
    worldAxisMaterial.diffuseColor = PRIMARY_YELLOW;
    // worldAxisMaterial.disableLighting = true;

    /*
    Obstacle preview materials
     */
    const obstaclePreviewBaseMaterial = new StandardMaterial(
        OBSTACLE_PREVIEW_BASE_MATERIAL_NAME,
        scene
    );
    obstaclePreviewBaseMaterial.emissiveColor = PRIMARY_YELLOW;
    obstaclePreviewBaseMaterial.alpha = OBSTACLE_PREVIEW_BASE_MATERIAL_ALPHA;
    obstaclePreviewBaseMaterial.disableLighting = true;
};
