import { ArcRotateCamera, Scene } from "@babylonjs/core";
import { Vector3 } from "@babylonjs/core/Maths/math.vector";
import { CAMERA_DEFAULT_ALPHA, CAMERA_DEFAULT_BETA, CAMERA_DEFAULT_RADIUS, CAMERA_LOWER_BETA_LIMIT, CAMERA_LOWER_RADIUS_LIMIT, CAMERA_PANNING_AXIS, CAMERA_PANNING_INERTIA, CAMERA_PANNING_SENSIBILITY, CAMERA_UPPER_BETA_LIMIT, CAMERA_UPPER_RADIUS_LIMIT, CAMERA_WHEEL_PRECISION } from "../../../BABYLON_CONST.ts";
import { MAIN_CAMERA_NAME } from "../../../NAME.ts";

export const enum CameraLayer {
  // MAIN_CAMERA_LAYER = parseInt("0xFFFFFFF"),
  MAIN_CAMERA_LAYER = 1,
  UI_CAMERA_LAYER = 2,
}

export const setupMainCamera = (scene: Scene): ArcRotateCamera => {
  const mainCamera = new ArcRotateCamera(
    MAIN_CAMERA_NAME,
    CAMERA_DEFAULT_ALPHA,
    CAMERA_DEFAULT_BETA,
    CAMERA_DEFAULT_RADIUS,
    Vector3.Up(),
    scene
  );
  mainCamera.layerMask = CameraLayer.MAIN_CAMERA_LAYER;
  mainCamera.panningAxis = CAMERA_PANNING_AXIS;
  mainCamera.upperBetaLimit = CAMERA_UPPER_BETA_LIMIT;
  mainCamera.lowerBetaLimit = CAMERA_LOWER_BETA_LIMIT;
  mainCamera.upperRadiusLimit = CAMERA_UPPER_RADIUS_LIMIT;
  mainCamera.lowerRadiusLimit = CAMERA_LOWER_RADIUS_LIMIT;
  mainCamera.attachControl(scene.getEngine().getRenderingCanvas(), true);
  mainCamera.wheelPrecision = CAMERA_WHEEL_PRECISION;
  mainCamera.panningInertia = CAMERA_PANNING_INERTIA;
  mainCamera.panningSensibility = CAMERA_PANNING_SENSIBILITY;
  return mainCamera;
};
