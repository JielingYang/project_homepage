import { Scene } from "@babylonjs/core";
import { MeshBuilder } from "@babylonjs/core/Meshes/meshBuilder";
import { GROUND_MESH_MATERIAL_NAME, GROUND_MESH_NAME } from "../../../NAME.ts";
import { CameraLayer, GROUND_SIZE } from "../../../BABYLON_CONST.ts";
import { ObjectTags } from "./tag.ts";
import { processCreatedMesh } from "../../../UTIL.ts";

export const setupGroundFlat = (scene: Scene) => {
  const groundMesh = MeshBuilder.CreateGround(
    GROUND_MESH_NAME,
    {
      width: GROUND_SIZE,
      height: GROUND_SIZE,
    },
    scene
  );

  groundMesh.material = scene.getMaterialByName(GROUND_MESH_MATERIAL_NAME);
  groundMesh.receiveShadows = true;
  groundMesh.enablePointerMoveEvents = true;
  groundMesh.layerMask = CameraLayer.MAIN_CAMERA_LAYER;
  
  processCreatedMesh(scene, groundMesh,[ObjectTags.SELECTABLE_TAG, ObjectTags.GRID_TAG], false, false);

  return groundMesh;
};
