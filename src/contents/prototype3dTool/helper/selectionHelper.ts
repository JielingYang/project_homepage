import { AbstractMesh, ActionManager, ExecuteCodeAction, Mesh, Scene, Tags } from "@babylonjs/core";
import { addTagsToTarget, ObjectTags, removeTagsFromTarget } from "./tag.ts";

const selectMesh = (scene: Scene, mesh: AbstractMesh) => {
    // if click on a selectable
    if (Tags.MatchesQuery(mesh, ObjectTags.SELECTABLE_TAG)) {
        // and it's selected already, deselect
        if (Tags.MatchesQuery(mesh, ObjectTags.SELECTED_TAG)) {
            deselectMesh(scene, mesh);
        }
        // otherwise deselect other and select this
        else {
            deselectAll(scene);
            addTagsToTarget(mesh, [ObjectTags.SELECTED_TAG]);

            mesh.getChildMeshes().forEach(m => addTagsToTarget(m, [ObjectTags.SELECTED_TAG]));
            const parent = mesh.parent;
            if (parent && (parent instanceof Mesh || parent instanceof AbstractMesh)) {
                addTagsToTarget(parent, [ObjectTags.SELECTED_TAG]);
            }
        }
    }

    // store.dispatch(
    //     setShowSelectablesMenuAction(
    //         scene.getMeshesByTags(ObjectTags.SELECTED_TAG).length > 0
    //     )
    // );
};

export const deselectMesh = (scene: Scene, mesh: AbstractMesh) => {
    // if click on a selectable and it's not selected
    if (
        Tags.MatchesQuery(
            mesh,
            `${ObjectTags.SELECTABLE_TAG} && ${ObjectTags.SELECTED_TAG}`
        )
    ) {
        removeTagsFromTarget(mesh, [ObjectTags.SELECTED_TAG]);

        mesh.getChildMeshes().forEach(m => removeTagsFromTarget(m, [ObjectTags.SELECTED_TAG]));
        const parent = mesh.parent;
        if (parent && (parent instanceof Mesh || parent instanceof AbstractMesh)) {
            removeTagsFromTarget(parent, [ObjectTags.SELECTED_TAG]);
        }
    }

    // store.dispatch(
    //     setShowSelectablesMenuAction(
    //         scene.getMeshesByTags(ObjectTags.SELECTED_TAG).length > 0
    //     )
    // );
};

export const deselectAll = (scene: Scene) => {
    scene
        .getMeshesByTags(ObjectTags.SELECTED_TAG)
        .forEach((m) => deselectMesh(scene, m));

    // store.dispatch(
    //     setShowSelectablesMenuAction(
    //         scene.getMeshesByTags(ObjectTags.SELECTED_TAG).length > 0
    //     )
    // );
};

export const setupPointerEnterMeshAction = (
    scene: Scene,
    mesh: AbstractMesh
) => {
    if (!mesh.actionManager) {
        mesh.actionManager = new ActionManager(scene);
    }
    mesh.actionManager.registerAction(
        new ExecuteCodeAction(
            {trigger: ActionManager.OnPointerOverTrigger},
            () => {
                // if enter a selectable and its not hovered
                if (
                    Tags.MatchesQuery(
                        mesh,
                        `${ObjectTags.SELECTABLE_TAG} && !${ObjectTags.HOVERED_TAG}`
                    )
                ) {
                    addTagsToTarget(mesh, [ObjectTags.HOVERED_TAG]);

                    mesh.getChildMeshes().forEach(m => addTagsToTarget(m, [ObjectTags.HOVERED_TAG]));
                    const parent = mesh.parent;
                    if (parent && (parent instanceof Mesh || parent instanceof AbstractMesh)) {
                        addTagsToTarget(parent, [ObjectTags.HOVERED_TAG]);
                    }
                }

            }
        )
    );
};

export const setupPointerExitMeshAction = (
    scene: Scene,
    mesh: AbstractMesh
) => {
    if (!mesh.actionManager) {
        mesh.actionManager = new ActionManager(scene);
    }
    mesh.actionManager.registerAction(
        new ExecuteCodeAction(
            {trigger: ActionManager.OnPointerOutTrigger},
            () => {
                // if exit a selectable
                if (Tags.MatchesQuery(mesh, ObjectTags.SELECTABLE_TAG)) {
                    removeTagsFromTarget(mesh, [ObjectTags.HOVERED_TAG]);

                    mesh.getChildMeshes().forEach(m => removeTagsFromTarget(m, [ObjectTags.HOVERED_TAG]));
                    const parent = mesh.parent;
                    if (parent && (parent instanceof Mesh || parent instanceof AbstractMesh)) {
                        removeTagsFromTarget(parent, [ObjectTags.HOVERED_TAG]);
                    }
                }
            }
        )
    );
};

export const setupPointerPickMeshAction = (
    scene: Scene,
    mesh: AbstractMesh
) => {
    if (!mesh.actionManager) {
        mesh.actionManager = new ActionManager(scene);
    }
    mesh.actionManager.registerAction(
        new ExecuteCodeAction({trigger: ActionManager.OnLeftPickTrigger}, () => {
            selectMesh(scene, mesh);
        })
    );
};
