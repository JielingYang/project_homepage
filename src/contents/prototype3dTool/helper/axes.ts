import { AbstractMesh, LinesMesh, Scene } from '@babylonjs/core'
import { Vector3 } from '@babylonjs/core/Maths/math.vector'
import { GRAY30_100, TRANSPARENT } from '../../../BABYLON_CONST.ts'
import { MeshBuilder } from '@babylonjs/core/Meshes/meshBuilder'
import { CUSTOM_AXES_NAME } from '../../../NAME.ts'
import { text2d } from './text2d.ts'

export const setupAxes = (scene: Scene, scale?: number, target?: AbstractMesh) => {
    const existingAxes = scene.getMeshByName(CUSTOM_AXES_NAME)
    if (existingAxes) {
        return existingAxes as LinesMesh
    } else {
        const s = scale ? scale : 1
        const points = [
            [Vector3.Zero(), Vector3.Right().scale(s)],
            [Vector3.Zero(), Vector3.Up().scale(s)],
            [Vector3.Zero(), Vector3.Forward().scale(s)],
        ]

        const colors = [
            [GRAY30_100, TRANSPARENT],
            [GRAY30_100, TRANSPARENT],
            [GRAY30_100, TRANSPARENT],
        ]

        const name = target ? `${CUSTOM_AXES_NAME}_${target.name}` : CUSTOM_AXES_NAME
        const customAxes = MeshBuilder.CreateLineSystem(name, { lines: points, colors: colors }, scene)
        customAxes.renderingGroupId = 1

        if (target) {
            customAxes.parent = target
            customAxes.position = target.getRawBoundingInfo().minimum
        }

        text2d(scene, 'x', customAxes, 50, 0.5, new Vector3(1, 0, -0.3), undefined, undefined, AbstractMesh.BILLBOARDMODE_ALL, 1)
        text2d(scene, 'y', customAxes, 50, 0.5, new Vector3(-0.3, 1, -0.3), undefined, undefined, AbstractMesh.BILLBOARDMODE_ALL, 1)
        text2d(scene, 'z', customAxes, 50, 0.5, new Vector3(-0.3, 0, 1), undefined, undefined, AbstractMesh.BILLBOARDMODE_ALL, 1)

        return customAxes
    }
}
