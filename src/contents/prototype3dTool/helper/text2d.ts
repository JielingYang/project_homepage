import { AbstractMesh, DynamicTexture, Mesh, Scene, StandardMaterial } from "@babylonjs/core";
import { MeshBuilder } from "@babylonjs/core/Meshes/meshBuilder";
import { GRAY20_100, TRANSPARENT } from "../../../BABYLON_CONST.ts";
import { Vector3 } from "@babylonjs/core/Maths/math.vector";

export const text2d = (scene: Scene, text: string, target: AbstractMesh, fontSize: number, planeHeight: number, position = Vector3.Zero(), fontColor = GRAY20_100, bgColor = TRANSPARENT, billboardMode = Mesh.BILLBOARDMODE_NONE, renderingGroupId = 0) => {
  const font = "bold " + fontSize + "px Arial";

  const DTHeight = 1.5 * fontSize;

  const ratio = planeHeight / DTHeight;

  //Use a temporary dynamic texture to calculate the length of the text on the dynamic texture canvas
  const tempDynamicTexture = new DynamicTexture("tempDynamicTexture", 64, scene);
  const tempDynamicTextureContext = tempDynamicTexture.getContext();
  tempDynamicTextureContext.font = font;
  const DTWidth = tempDynamicTextureContext.measureText(text).width + 8;
  tempDynamicTexture.dispose();

  //Calculate width the plane has to be
  const planeWidth = DTWidth * ratio;

  //Create dynamic texture and write the text
  const dynamicTexture = new DynamicTexture(`textDynamicTexture_${text}`, { width: DTWidth, height: DTHeight }, scene, false);
  dynamicTexture.drawText(text, null, null, font, fontColor.toHexString(), bgColor.toHexString(), true);

  const mat = new StandardMaterial(`textMaterial_${text}`, scene);
  mat.diffuseTexture = dynamicTexture;
  mat.diffuseTexture.hasAlpha = true;

  //Create plane and set dynamic texture as material
  const textPlane = MeshBuilder.CreatePlane(`textPlane_${text}`, { width: planeWidth, height: planeHeight }, scene);
  textPlane.renderingGroupId = renderingGroupId;
  textPlane.material = mat;
  textPlane.billboardMode = billboardMode;
  textPlane.parent = target;
  textPlane.position = position;
};
