import { AbstractMesh, Mesh, Tags } from "@babylonjs/core";
import { SPACE_STRING } from "../../../NAME.ts";

// Tag
export const enum ObjectTags {
    RENDER_REQUIRED_TAG = "renderRequired", // Do not manually add/remove this tag
    SELECTABLE_TAG = "selectable",
    SELECTED_TAG = "selected",
    HOVERED_TAG = "hovered",
    GRID_TAG = "grid",
}

export const addTagsToTarget = (
    target: Mesh | AbstractMesh,
    tags: Array<ObjectTags>
) => {
    if (tags.includes(ObjectTags.RENDER_REQUIRED_TAG)) {
        throw new Error(
            "ObjectTags.RENDER_REQUIRED_TAG should not be added manually"
        );
    }
    !Tags.HasTags(target) && Tags.EnableFor(target);
    Tags.AddTagsTo(target, tags.join(SPACE_STRING));

    !Tags.MatchesQuery(target, ObjectTags.RENDER_REQUIRED_TAG) &&
    Tags.AddTagsTo(target, ObjectTags.RENDER_REQUIRED_TAG);
};

export const removeTagsFromTarget = (
    target: Mesh | AbstractMesh,
    tags: Array<ObjectTags>
) => {
    if (tags.includes(ObjectTags.RENDER_REQUIRED_TAG)) {
        throw new Error(
            "ObjectTags.RENDER_REQUIRED_TAG should not be removed manually"
        );
    }
    if (Tags.HasTags(target)) {
        Tags.RemoveTagsFrom(target, tags.join(SPACE_STRING));

        !Tags.MatchesQuery(target, ObjectTags.RENDER_REQUIRED_TAG) &&
        Tags.AddTagsTo(target, ObjectTags.RENDER_REQUIRED_TAG);
    }
};

export const markTargetAsRendered = (target: Mesh | AbstractMesh) => {
    if (Tags.HasTags(target)) {
        Tags.MatchesQuery(target, ObjectTags.RENDER_REQUIRED_TAG) &&
        Tags.RemoveTagsFrom(target, ObjectTags.RENDER_REQUIRED_TAG);
    }
};
