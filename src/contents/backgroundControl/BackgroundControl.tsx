import { ChangeEvent, CSSProperties, useCallback, useRef } from "react";
import { useIntersectRatio } from "../../hooks/hooks.ts";
import { DEFAULT_BORDER_RADIUS, HEAVY_BLUR, XLARGE_PADDING_IN_VW, XXLARGE_PADDING_IN_VW } from "../../UI_CONST.ts";
import { Box, Button, Grow, Slider, Stack, Switch, SxProps, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { setInteractionPoint, setTilesBackfaceVisible, setTilesBorderRadius, setTilesBorderWidth } from "../../redux/reducers/interactiveTilesStateReducer.ts";
import { TRootState } from "../../redux/store.ts";
import TouchAppIcon from "@mui/icons-material/TouchApp";
import { generateRandomInteractionPoint } from "../../interactiveTilesBackground/interactiveTilesBackgroundUtils.ts";

const rootCss: CSSProperties = {
  position: "relative",
  boxSizing: "border-box",
  width: "100%",
  height: "100vh",
  display: "flex",
  // backgroundColor: 'rgba(0,0,0,0.2)',
  justifyContent: "space-between",
  alignItems: "center",
  padding: "5vw"
};
const textSectionCss: CSSProperties = {
  position: "relative",
  boxSizing: "border-box",
  maxWidth: "48%",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  rowGap: `${XLARGE_PADDING_IN_VW}vw`,
  padding: `${XXLARGE_PADDING_IN_VW}vw`,
  textAlign: "center",
  backgroundColor: "rgba(0,0,0,0.2)",
  borderRadius: DEFAULT_BORDER_RADIUS,
  backdropFilter: HEAVY_BLUR
};
const controlSectionCss: CSSProperties = {
  position: "relative",
  boxSizing: "border-box",
  width: "48%",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  height: "100%"
};

const controlSx: SxProps = {
  width: "10vw",
  display: "flex",
  justifyContent: "center"
};
const controlTitleSx: SxProps = {
  width: "20vw",
  textAlign: "right"
};

const BackgroundControl = (() => {
  const dispatch = useDispatch();
  const ref = useRef<HTMLDivElement>(null);
  const panelIntersectRatio = useIntersectRatio(ref);

  const tilesBorderRadius = useSelector((state: TRootState) => state.interactiveTilesState.tilesBorderRadius);
  const tilesBorderWidth = useSelector((state: TRootState) => state.interactiveTilesState.tilesBorderWidth);
  const tilesBackfaceVisibility = useSelector((state: TRootState) => state.interactiveTilesState.tilesBackfaceVisibility);

  const handleBorderRadiusChange = useCallback((e: Event) => {
    const target = e.target as HTMLInputElement;
    dispatch(setTilesBorderRadius(Number(target.value)));
  }, [dispatch]);
  const handleBorderWidthChange = useCallback((e: Event) => {
    const target = e.target as HTMLInputElement;
    dispatch(setTilesBorderWidth(Number(target.value)));
  }, [dispatch]);
  const handleBackfaceVisibilityChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    dispatch(setTilesBackfaceVisible(e.target.checked ? "visible" : "hidden"));
  }, [dispatch]);

  return (
    <div id={"backgroundControl"} ref={ref} style={rootCss}>
      <Grow in={panelIntersectRatio > 0.7} timeout={1500}>
        <div style={controlSectionCss}>
          <Stack spacing={"2vw"} direction="row" alignItems="center">
            <Typography sx={controlTitleSx} variant={"body2"}>Shape</Typography>
            <Slider
              color={"secondary"}
              sx={controlSx}
              id={"borderRadiusSlider"}
              onChange={handleBorderRadiusChange}
              value={tilesBorderRadius}
              step={1}
              min={0}
              max={50}
            />
          </Stack>
          <Stack spacing={"2vw"} direction="row" alignItems="center">
            <Typography sx={controlTitleSx} variant={"body2"}>Border width</Typography>
            <Slider
              color={"secondary"}
              sx={controlSx}
              id={"borderWidthSlider"}
              onChange={handleBorderWidthChange}
              value={tilesBorderWidth}
              step={0.5}
              min={0}
              max={10}
            />
          </Stack>
          <Stack spacing={"2vw"} direction="row" alignItems="center">
            <Typography sx={controlTitleSx} variant={"body2"}>Backface visibility</Typography>
            <Box sx={controlSx}>
              <Switch
                color={"secondary"} value={tilesBackfaceVisibility === "hidden"} onChange={handleBackfaceVisibilityChange} />
            </Box>
          </Stack>
          <Stack spacing={"2vw"} direction="row" alignItems="center">
            <Typography sx={controlTitleSx} variant={"body2"}>Action time!</Typography>
            <Box sx={controlSx}>
              <Button onClick={() => dispatch(setInteractionPoint(generateRandomInteractionPoint()))} size={"small"} variant={"contained"} color={"secondary"}><TouchAppIcon /></Button>
            </Box>
          </Stack>
        </div>
      </Grow>
      <div style={textSectionCss}>
        <Grow in={panelIntersectRatio > 0.4} timeout={800}>
          <Typography variant={"body2"}>You probably have noticed, the background is dynamic!</Typography>
        </Grow>
        <Grow in={panelIntersectRatio > 0.5} timeout={1000}>
          <Typography variant={"body2"}>It is made of bunch of div elements which are transformed based on their indices.</Typography>
        </Grow>
        <Grow in={panelIntersectRatio > 0.6} timeout={1200}>
          <Typography variant={"body2"}>Now try out the UI on the left to interact with the background!</Typography>
        </Grow>
      </div>
    </div>
  );
});

export default BackgroundControl;
