import { CSSProperties, useRef } from "react";
import { useIntersectRatio } from "../hooks/hooks";

const rootCss: CSSProperties = {
    position: 'relative',
    boxSizing: 'border-box',
    width: '100%',
    minHeight: '100vh',
    display: 'flex',
    backgroundColor: 'rgba(0,0,0,0.2)',
}

const Temp = (() => {
    const ref = useRef<HTMLDivElement>(null);
    const panelIntersectRatio = useIntersectRatio(ref)

    console.log('temp', panelIntersectRatio)
    return <div id={'temp'} ref={ref} style={rootCss}></div>
})

export default Temp
