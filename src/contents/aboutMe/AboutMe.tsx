import { CSSProperties, useMemo, useRef } from "react";
import { useIntersectRatio } from "../../hooks/hooks.ts";
import { DEFAULT_BORDER_RADIUS, HEAVY_BLUR, XLARGE_PADDING_IN_VW, XXLARGE_PADDING_IN_VW } from "../../UI_CONST.ts";
import DivCube from "./DivCube.tsx";
import { CUBE_LEFT_IN_VW, CUBE_SIZE_IN_VW } from "../contentsUtils.ts";
import { Box, Grow, SxProps, Typography } from "@mui/material";
import { keyframes } from "@emotion/react";

const fadeInOut = keyframes({
    '0%': { opacity: 0.8 },
    '20%': { opacity: 0.9 },
    '21%': { opacity: 0.7 },
    '30%': { opacity: 1 },
    '31%': { opacity: 0.8 },
    '90%': { opacity: 0.9 },
    '92%': { opacity: 1 },
    '100%': { opacity: 0.8 },
})

const riseAndFade = keyframes({
    '0%': { opacity: 0, transform: 'translateY(0) scale(1,1)' },
    '15%': { opacity: 1, transform: 'translateY(-10vh) scale(0.5,0.5)' },
    '30%': { opacity: 0, transform: 'translateY(-20vh) scale(0,0)' },
    '100%': { opacity: 0, transform: 'translateY(-20vh) scale(0,0)' },
})

const rootCss: CSSProperties = {
    position: 'relative',
    boxSizing: 'border-box',
    width: '100%',
    minHeight: '100vh',
    display: 'flex',
    // backgroundColor: 'rgba(0,0,0,0.2)',
}

const panelBackgroundCss: CSSProperties = {
    boxSizing: 'border-box',
    position: 'absolute',
    width: '90vw',
    height: '40vw',
    minWidth: '921px',
    minHeight: '409px',
    backgroundColor: 'rgba(0,0,0,0.2)',
    borderRadius: DEFAULT_BORDER_RADIUS,
    left: '50%',
    top: '50%',
    transform: 'translate(-50%,-50%)',
}

const panelBlurCss: CSSProperties = {
    position: 'absolute',
    width: '100%',
    height: '100%',
    borderRadius: DEFAULT_BORDER_RADIUS,
    backdropFilter: HEAVY_BLUR,
}

const panelTextContainerCss: CSSProperties = {
    boxSizing: 'border-box',
    position: 'absolute',
    width: `${CUBE_LEFT_IN_VW}vw`,
    height: '100%',
    left: 0,
    top: 0,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    rowGap: `${XLARGE_PADDING_IN_VW}vw`,
    padding: `${XXLARGE_PADDING_IN_VW}vw`,
    textAlign: 'center',
}

const perspectivePanelCss: CSSProperties = {
    ...panelBackgroundCss,
    overflow: 'visible',
    backgroundColor: 'none',
    perspectiveOrigin: '60% 70%',
    perspective: '1500px',
    transformStyle: 'preserve-3d',
}

const cubeBaseCss: CSSProperties = {
    left: `${CUBE_LEFT_IN_VW}vw`,
    bottom: '-6vh',
    boxSizing: 'border-box',
    position: 'absolute',
    width: `${CUBE_SIZE_IN_VW}vw`,
    height: `${CUBE_SIZE_IN_VW}vw`,
    // backgroundColor: 'black',
    backfaceVisibility: 'hidden',
    transform: 'rotateX(90deg)',
    borderRadius: '50%',
    display: 'flex',
    transformStyle: 'preserve-3d',
    pointerEvents: 'none',
}

const cubeBaseInnerCss: CSSProperties = {
    boxSizing: 'border-box',
    position: 'relative',
    width: '20%',
    height: '20%',
    backgroundColor: 'rgb(0,255,255)',
    backfaceVisibility: 'hidden',
    borderRadius: '50%',
    boxShadow: '0 0 5vw 3vw rgb(0,255,255)',
    transformStyle: 'preserve-3d',
    margin: 'auto',
}

const cubeBaseInnerLightBasicSx: SxProps = {
    boxSizing: 'border-box',
    position: 'absolute',
    width: '500%',
    height: '700%',
    left: '50%',
    top: '50%',
    transformOrigin: 'center top',
    // backgroundColor: 'red',
    background: 'linear-gradient(rgb(0,255,255, 0.5),rgba(0,0,0,0) 80%)',
    clipPath: 'polygon(40% 0, 60% 0, 100% 100%, 0 100%)',
    animation: `${fadeInOut} 2s infinite alternate`,
}

const floatingDotsSx: SxProps = {
    position: 'absolute',
    backgroundColor: 'rgb(0,255,255)',
    boxShadow: '0 0 2vw 1vw rgb(0,255,255)',
    borderRadius: '50%',
    width: '0.2vmin',
    height: '0.2vmin',
    opacity: 0,
    pointerEvents: 'none',
}

const floatingDot1Sx: SxProps = {
    ...floatingDotsSx,
    left: `${CUBE_LEFT_IN_VW}vw`,
    top: '40vh',
    animation: `${riseAndFade} 10s linear infinite`,
}

const floatingDot2Sx: SxProps = {
    ...floatingDotsSx,
    left: `${CUBE_LEFT_IN_VW + CUBE_SIZE_IN_VW}vw`,
    top: '30vh',
    animation: `${riseAndFade} 12s linear 2s infinite`,
}

const floatingDot3Sx: SxProps = {
    ...floatingDotsSx,
    left: `${CUBE_LEFT_IN_VW - 2}vw`,
    top: '6vh',
    animation: `${riseAndFade} 8s linear 3s infinite`,
}

const floatingDot4Sx: SxProps = {
    ...floatingDotsSx,
    left: `${CUBE_LEFT_IN_VW + CUBE_SIZE_IN_VW + 5}vw`,
    top: '15vh',
    animation: `${riseAndFade} 14s linear 6s infinite`,
}

const displayNoneCss: CSSProperties = {
    display: 'none',
}

// @ts-ignore
const AboutMe = (() => {
    const ref = useRef<HTMLDivElement>(null);
    const panelIntersectRatio = useIntersectRatio(ref)

    const cubeBaseInnerLightSx: SxProps = useMemo(
        () => ({
            ...cubeBaseInnerLightBasicSx,
            opacity: panelIntersectRatio,
            transform: `rotateX(90deg) translateX(-50%) scaleX(${panelIntersectRatio})`,
        }),
        [panelIntersectRatio]
    )

    // console.log('AboutMe', panelIntersectRatio)
    return (
        <div id={'aboutMe'} ref={ref} style={rootCss}>
            <div style={panelIntersectRatio === 0 ? displayNoneCss : panelBackgroundCss}>
                <div style={panelBlurCss} />
                <div style={panelTextContainerCss}>
                    <Grow in={panelIntersectRatio > 0.7} timeout={1000}>
                        <Typography variant={'h1'}>{"Hi there! I'm Jieling"}</Typography>
                    </Grow>
                    <Grow in={panelIntersectRatio > 0.9} timeout={1200}>
                        <Typography variant={'body2'}>
                            {
                                "This is a place where I try out new thoughts and experiment with fun technologies. I'm sure you'll find something interesting! So, keep scrolling and have fun!"
                            }
                        </Typography>
                    </Grow>
                </div>
            </div>
            <div style={perspectivePanelCss}>
                <DivCube
                    intersectionRatio={panelIntersectRatio}
                    cubeId={'cube'}
                    size={`${CUBE_SIZE_IN_VW}vw`}
                    left={`${CUBE_LEFT_IN_VW}vw`}
                    top={'5vh'}
                />
                <div style={cubeBaseCss}>
                    <div style={cubeBaseInnerCss}>
                        <Box sx={cubeBaseInnerLightSx} />
                    </div>
                </div>
                <Box sx={floatingDot1Sx} />
                <Box sx={floatingDot2Sx} />
                <Box sx={floatingDot3Sx} />
                <Box sx={floatingDot4Sx} />
            </div>
        </div>
    )
})

export default AboutMe
