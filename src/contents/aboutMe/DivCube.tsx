import { CSSProperties, useMemo, useState } from "react";
import { Property } from "csstype";
import { Box, SxProps } from "@mui/material";
import { randomNumFromInterval } from "../../UTIL.ts";

const cubeCoreBasicSx: SxProps = {
    boxSizing: 'border-box',
    position: 'absolute',
    transformStyle: 'preserve-3d',
    cursor: 'pointer',
    transition: 'transform 5s',
    '&:hover': {
        '& .innerFace': {
            backgroundColor: 'rgb(0,255,255)',
            boxShadow: '0 0 2.1vw 0.6vw rgba(0,255,255,0.9)',
        },
    },
}

const cubeFaceBasicCss: CSSProperties = {
    boxSizing: 'border-box',
    position: 'absolute',
    width: '100%',
    height: '100%',
    padding: '2vw',
    backgroundColor: 'rgb(70,21,0)',
    backfaceVisibility: 'hidden',
}

const cubeLeftFaceBasicCss: CSSProperties = {
    ...cubeFaceBasicCss,
    top: 0,
    left: '-50%',
    transform: 'rotateY(-90deg)',
}

const cubeRightFaceBasicCss: CSSProperties = {
    ...cubeFaceBasicCss,
    top: 0,
    right: '-50%',
    transform: 'rotateY(90deg)',
}

const cubeTopFaceBasicCss: CSSProperties = {
    ...cubeFaceBasicCss,
    top: 0,
    right: 0,
    transform: 'translateY(-50%) rotateX(90deg)',
}

const cubeBottomFaceBasicCss: CSSProperties = {
    ...cubeFaceBasicCss,
    top: 0,
    right: 0,
    transform: 'translateY(50%) rotateX(-90deg)',
}

const cubeFrontFaceBasicCss: CSSProperties = {
    ...cubeFaceBasicCss,
    top: 0,
    right: 0,
    transform: 'rotateY(90deg) translateX(-50%) rotateY(-90deg)',
}

const cubeBackFaceBasicCss: CSSProperties = {
    ...cubeFaceBasicCss,
    top: 0,
    right: 0,
    transform: 'rotateY(90deg) translateX(50%) rotateY(90deg)',
}

const innerFaceSx: SxProps = {
    boxSizing: 'border-box',
    width: '100%',
    height: '100%',
    borderRadius: '0.5vw',
    transition: 'background-color 0.5s, box-shadow 0.5s',
    backgroundColor: 'rgb(0,190,200)',
    boxShadow: '0 0 2.1vw 0.5vw rgba(0,255,255,0.8)',
}

interface IDivCube {
    cubeId: string
    left?: Property.Left
    right?: Property.Right
    top?: Property.Top
    bottom?: Property.Bottom
    intersectionRatio: number
    size: Property.Width
}

function DivCube({ cubeId, top, left, bottom, right, intersectionRatio, size }: IDivCube) {
    const [cubeClickToggle, setCubeClickToggle] = useState(false)
    const triggerRotation = useMemo(() => intersectionRatio >= 0.7, [intersectionRatio])

    const cubeCoreSx: SxProps = useMemo(() => {
        return {
            ...cubeCoreBasicSx,
            width: size,
            height: size,
            top: top ? top : 'auto',
            bottom: bottom ? bottom : 'auto',
            left: left ? left : 'auto',
            right: right ? right : 'auto',
            transform: `rotateX(${45}deg) rotateY(${0}deg) rotateZ(${45}deg)`,
        }
    }, [bottom, left, right, size, top])

    const cubeCoreRotatedSx: SxProps = useMemo(
        () => {
            if (triggerRotation) {
                const xRotation = randomNumFromInterval(360, 720)
                const yRotation = randomNumFromInterval(360, 720)
                const zRotation = randomNumFromInterval(360, 720)
                return {
                    ...cubeCoreSx,
                    transform: `rotateX(${xRotation}deg) rotateY(${yRotation}deg) rotateZ(${zRotation}deg)`,
                }
            } else {
                return cubeCoreSx
            }
        },
        // Do not remove cubeClickToggle, it is used to trigger the rotation by clicking
        [cubeCoreSx, triggerRotation, cubeClickToggle]
    )

    return (
        <Box
            id={`${cubeId}_core`}
            sx={triggerRotation ? cubeCoreRotatedSx : cubeCoreSx}
            onClick={() => setCubeClickToggle((prev) => !prev)}
        >
            <div id={`${cubeId}_leftFace`} style={cubeLeftFaceBasicCss}>
                <Box className={'innerFace'} sx={innerFaceSx} />
            </div>
            <div id={`${cubeId}_rightFace`} style={cubeRightFaceBasicCss}>
                <Box className={'innerFace'} sx={innerFaceSx} />
            </div>
            <div id={`${cubeId}_frontFace`} style={cubeFrontFaceBasicCss}>
                <Box className={'innerFace'} sx={innerFaceSx} />
            </div>
            <div id={`${cubeId}_backFace`} style={cubeBackFaceBasicCss}>
                <Box className={'innerFace'} sx={innerFaceSx} />
            </div>
            <div id={`${cubeId}_bottomFace`} style={cubeBottomFaceBasicCss}>
                <Box className={'innerFace'} sx={innerFaceSx} />
            </div>
            <div id={`${cubeId}_topFace`} style={cubeTopFaceBasicCss}>
                <Box className={'innerFace'} sx={innerFaceSx} />
            </div>
        </Box>
    )
}

export default DivCube
