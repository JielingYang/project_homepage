import { CSSProperties } from "react";
import AboutMe from "./aboutMe/AboutMe.tsx";
import MySkills from "./mySkills/MySkills.tsx";
import BackgroundControl from "./backgroundControl/BackgroundControl.tsx";
import ToolContainer from "./prototype3dTool/ToolContainer.tsx";

const contentsContainerCss: CSSProperties = {
  boxSizing: "border-box",
  boxShadow: "none",
  position: "absolute",
  // width: `calc(100% - ${NAVIGATOR_SHRUNK_WIDTH_IN_PX}px)`,
  // left: `${NAVIGATOR_SHRUNK_WIDTH_IN_PX}px`,
  width: "100%",
  left: 0,
  top: 0,
  height: "100%",
  overflowY: "auto",
  overflowX: "clip"
};

const dummyCss: CSSProperties = { background: "red", width: "10vw", height: "100vh", margin: "auto" };

function ContentsContainer() {

  console.log("contentsContainer");
  return (
    <div id={"contentsContainer"} style={contentsContainerCss}>
      <ToolContainer />
      <AboutMe />
      <MySkills />
      <BackgroundControl />
      <div style={dummyCss} />

      {/*<Navigator contentsRefs={contentsRefs} />*/}
    </div>
  );
}

export default ContentsContainer;
