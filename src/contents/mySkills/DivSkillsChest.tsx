import { Box, Button, Slide, SxProps, Typography } from "@mui/material";
import { CSSProperties, useCallback, useEffect, useMemo, useRef, useState } from "react";
import { keyframes } from "@emotion/react";
import { CHEST_HEIGHT_IN_VW } from "../../UI_CONST.ts";
import DivSkillsChestCards from "./DivSkillsChestCards.tsx";
import { useDispatch, useSelector } from "react-redux";
import { setInteractionPoint } from "../../redux/reducers/interactiveTilesStateReducer.ts";
import { generateRandomInteractionPoint } from "../../interactiveTilesBackground/interactiveTilesBackgroundUtils.ts";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import { TRootState } from "../../redux/store.ts";
import { setCardContent } from "../../redux/reducers/skillsChestStateReducer.ts";

const boxShadow2 = "0 0 0.8vw 0.08vw rgba(0,0,0,0.5)";
const boxShadow3 = "inset 0 0 15vw 10vw rgb(0,0,0,0.8)";
const boxShadow4 = "0 0 0.6vw 0.5vw rgba(0,0,0,0.6), inset 0 0 4vw 1vw rgb(0,0,0,1)";
const boxShadow5 = "0 0 0.6vw 0.5vw rgba(0,0,0,0.6), inset 0 0 10vw 3vw rgb(0,0,0,1)";

const flashColor1 = "rgb(120,120,120)";
const flashColor2 = "rgb(122,122,122)";
const flashShadow1 = "0 0 0.5vw 0.1vw rgba(160,160,160,1)";
const flashShadow2 = "0 0 0.55vw 0.1vw rgba(162,162,162,1)";

const chestWidthInVw = 37;
const chestDepthInVw = 30;
const chestWallThicknessInVw = 0.6;

const circleBaseSizeInVw = chestWidthInVw * 1.5;

const rotate = keyframes({
  "0%": { transform: "rotateX(90deg) rotateZ(0)" },
  "100%": { transform: "rotateX(90deg) rotateZ(360deg)" }
});

const flash = keyframes({
  "0%": { backgroundColor: flashColor1, boxShadow: flashShadow1 },
  "20%": { backgroundColor: flashColor2, boxShadow: flashShadow2 },
  "21%": { backgroundColor: flashColor1, boxShadow: flashShadow1 },
  "85%": { backgroundColor: flashColor1, boxShadow: flashShadow2 },
  "90%": { backgroundColor: flashColor2, boxShadow: flashShadow1 },
  "92%": { backgroundColor: flashColor1, boxShadow: flashShadow2 },
  "100%": { backgroundColor: flashColor2, boxShadow: flashShadow1 }
});

const skillsChestCoreBasicSx: SxProps = {
  boxSizing: "border-box",
  position: "absolute",
  transformStyle: "preserve-3d",
  width: `${chestWidthInVw}vw`,
  height: `${CHEST_HEIGHT_IN_VW}vw`,
  bottom: "25vh",
  left: "12vw"
};

const skillsChestCircleBaseSx: SxProps = {
  boxSize: "border-box",
  boxSizing: "border-box",
  position: "absolute",
  transformStyle: "preserve-3d",
  width: `${circleBaseSizeInVw}vw`,
  height: `${circleBaseSizeInVw}vw`,
  borderRadius: "50%",
  backfaceVisibility: "hidden",
  left: `${(chestWidthInVw - circleBaseSizeInVw) * 0.5}vw`,
  bottom: `${-circleBaseSizeInVw * 0.5}vw`,
  transform: `rotateX(90deg)`,
  border: `2vw dashed rgb(8,51,43)`,
  animation: `${rotate} 300s linear infinite alternate`
};

const skillsChestBasicFaceASx: SxProps = {
  backfaceVisibility: "hidden",
  boxSizing: "border-box",
  position: "absolute",
  transformStyle: "preserve-3d"
};

const skillsChestBasicFaceBBrightSx: SxProps = {
  ...skillsChestBasicFaceASx,
  left: `${chestWallThicknessInVw}vw`,
  top: `${chestWallThicknessInVw}vw`,
  width: `calc(100% - ${2 * chestWallThicknessInVw}vw)`,
  height: `calc(100% - ${2 * chestWallThicknessInVw}vw)`,
  backgroundColor: flashColor1,
  borderRadius: "0.5vw",
  boxShadow: flashShadow1,
  animation: `${flash} 3s linear infinite alternate`,
  padding: "1vw",
  paddingBottom: 0,
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between"
};

const skillsChestBasicFaceBDarkSx: SxProps = {
  ...skillsChestBasicFaceBBrightSx,
  backgroundColor: "rgba(0,70,82,0.9)",
  boxShadow: boxShadow4,
  animation: "none"
};

const skillsChestBackLidSx: SxProps = {
  ...skillsChestBasicFaceBBrightSx,
  backgroundColor: "rgba(0,58,66,0.9)",
  boxShadow: boxShadow5,
  animation: "none"
};

const skillsChestOuterFrontFaceASx: SxProps = {
  ...skillsChestBasicFaceASx,
  backgroundColor: "rgb(65,18,1)",
  width: `${chestWidthInVw}vw`,
  height: `${CHEST_HEIGHT_IN_VW}vw`,
  transform: `translateZ(${chestDepthInVw * 0.5}vw)`
};

const skillsChestTopFaceSx: SxProps = {
  ...skillsChestBasicFaceASx,
  backgroundColor: "rgba(0,0,0,1)",
  width: `${chestWidthInVw}vw`,
  height: `${chestDepthInVw}vw`,
  border: `${chestWallThicknessInVw}vw solid rgb(68,29,0)`,
  transform: `translateY(-${chestDepthInVw * 0.5}vw) translateY(0.5px) rotateX(90deg)`,
  transition: "background-color 1s ease-in-out",
};

const skillsChestTopFacePointerEventsNoneSx: SxProps = {
  ...skillsChestTopFaceSx,
  pointerEvents: "none",
  backgroundColor: "rgba(0,0,0,0)"
};

const skillsChestBottomFaceSx: SxProps = {
  ...skillsChestBasicFaceASx,
  width: `${chestWidthInVw - 2 * chestWallThicknessInVw}vw`,
  height: `${chestDepthInVw - 2 * chestWallThicknessInVw}vw`,
  left: `${chestWallThicknessInVw}vw`,
  bottom: 0,
  backgroundColor: "rgb(4,26,28)",
  boxShadow: boxShadow3,
  transform: `translateY(${(chestDepthInVw - 2 * chestWallThicknessInVw) * 0.5}vw) rotateX(90deg)`
};

const skillsChestLidFrontClosedSx: SxProps = {
  ...skillsChestBasicFaceASx,
  backgroundColor: "rgb(115,44,0)",
  left: `${chestWallThicknessInVw}vw`,
  bottom: 0,
  width: `${chestWidthInVw - 2 * chestWallThicknessInVw}vw`,
  height: `${chestDepthInVw - 2 * chestWallThicknessInVw}vw`,
  transformOrigin: "bottom center",
  transform: `translateY(-${CHEST_HEIGHT_IN_VW}vw) translateZ(-${chestDepthInVw * 0.5 - chestWallThicknessInVw}vw) rotateX(-90deg)`,
  transition: "transform 1s"
};

const skillsChestLidFrontOpenedSx: SxProps = {
  ...skillsChestLidFrontClosedSx,
  transform: `translateY(-${CHEST_HEIGHT_IN_VW}vw) translateZ(-${chestDepthInVw * 0.5 - chestWallThicknessInVw}vw) rotateX(5deg)`
};

const skillsChestLidBackSx: SxProps = {
  ...skillsChestBasicFaceASx,
  backgroundColor: "rgb(51,19,3)",
  width: `${chestWidthInVw - 2 * chestWallThicknessInVw}vw`,
  height: `${chestDepthInVw - 2 * chestWallThicknessInVw}vw`,
  left: "50%",
  top: "50%",
  transform: "translate(-50%, -50%) rotateX(180deg)"
};

const skillsChestOuterBackFaceSx: SxProps = {
  ...skillsChestBasicFaceASx,
  backgroundColor: "none",
  width: `${chestWidthInVw}vw`,
  height: `${CHEST_HEIGHT_IN_VW}vw`,
  backfaceVisibility: "visible",
  boxShadow: boxShadow2,
  transform: `translateZ(${-chestDepthInVw * 0.5}vw) rotateY(180deg)`
};

const skillsChestInnerBackFaceSx: SxProps = {
  ...skillsChestBasicFaceASx,
  width: `${chestWidthInVw - 2 * chestWallThicknessInVw}vw`,
  height: `${CHEST_HEIGHT_IN_VW}vw`,
  left: `${chestWallThicknessInVw}vw`,
  transform: `translateZ(${-chestDepthInVw * 0.5 + chestWallThicknessInVw}vw)`,
  backgroundColor: "rgb(26,8,1)"
};

const skillsChestInnerRightFaceSx: SxProps = {
  ...skillsChestBasicFaceASx,
  width: `${chestDepthInVw - 2 * chestWallThicknessInVw}vw`,
  height: `${CHEST_HEIGHT_IN_VW}vw`,
  right: `${-((chestDepthInVw - 2 * chestWallThicknessInVw) * 0.5 - chestWallThicknessInVw)}vw`,
  transform: `rotateY(-90deg)`,
  backgroundColor: "rgb(16,8,0)"
};

const skillsChestOuterLeftFaceSx: SxProps = {
  ...skillsChestOuterFrontFaceASx,
  width: `${chestDepthInVw}vw`,
  height: `${CHEST_HEIGHT_IN_VW}vw`,
  left: `${-chestDepthInVw * 0.5}vw`,
  transform: `rotateY(-90deg)`
};

const skillsChestOuterRightFaceSx: SxProps = {
  ...skillsChestOuterFrontFaceASx,
  width: `${chestDepthInVw}vw`,
  height: `${CHEST_HEIGHT_IN_VW}vw`,
  left: "auto",
  right: `${-chestDepthInVw * 0.5}vw`,
  transform: `rotateY(90deg)`,
  backgroundColor: "none",
  boxShadow: boxShadow2,
  backfaceVisibility: "visible"
};

const buttonSx: SxProps = {
  position: "absolute",
  left: "1vw",
  top: "1vw",
  color: "#b4b4b4",
  backgroundColor: "rgb(79,79,79)",
  "&:hover": {
    backgroundColor: "#343434"
  }
};

const arrowsAndTitlesWrapperSx: SxProps = {
  position: "relative",
  display: "flex",
  flexDirection: "column",
  flexGrow: 0
};

const arrowsWrapperSx: SxProps = {
  position: "relative",
  display: "flex",
  justifyContent: "space-between",
  color: "#4f4f4f"
};

const arrowSx: SxProps = {
  fontSize: "3vw"
};

const arrowTitlesWrapperSx: SxProps = {
  position: "relative",
  display: "flex",
  justifyContent: "space-between",
  paddingTop: "1vw",
  borderTop: "1px solid #41414135"
};

const headerWrapperSx: SxProps = {
  boxSizing: "border-box",
  position: "relative",
  display: "flex",
  justifyContent: "center",
  minHeight: "3vw",
  borderBottom: "1px solid #41414135"
};

const verticalContentWrapperSx: SxProps = {
  position: "relative",
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-evenly",
  rowGap: "1vw",
  flexGrow: 1
};

const horizontalContentWrapperSx: SxProps = {
  ...verticalContentWrapperSx,
  flexDirection: "row",
  alignItems: "center"
};

const partitionCss: CSSProperties = {
  position: "relative",
  width: "45%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center"
};

const logoCss: CSSProperties = {
  position: "relative",
  boxSizing: "border-box",
  margin: "auto",
  width: "15vw",
  height: "15vw",
  filter: "opacity(50%) saturate(0) grayscale(100%)"
};

const verticalLineCss: CSSProperties = {
  borderRight: "1px solid #41414135",
  height: "80%"
};

const startingContent = <Box sx={verticalContentWrapperSx}>
  <Typography variant={"h6"} align={"center"}>
    As a full stack developer, here are my most commonly used languages and technologies, also areas I normally work on
  </Typography>
  <Typography variant={"h6"} align={"center"}>
    Try click on them to see more!
  </Typography>
</Box>;

interface IDivSkillsChest {
  intersectionRatio: number;
}

function DivSkillsChest({ intersectionRatio }: IDivSkillsChest) {
  const titleToShow = useSelector((state: TRootState) => state.skillsChestState.titleToShow);
  const textToShow = useSelector((state: TRootState) => state.skillsChestState.textToShow);
  const logoToShow = useSelector((state: TRootState) => state.skillsChestState.logoToShow);
  const dispatch = useDispatch();
  const buttonContainerRef = useRef(null);
  const [skillsChestLidOpen, setSkillsChestLidOpen] = useState(false);

  const skillsChestCoreSx: SxProps = useMemo(
    () => ({
      ...skillsChestCoreBasicSx,
      transform: `rotateX(-7deg) rotateY(${45 * intersectionRatio}deg)`
    }),
    [intersectionRatio]
  );

  const openChest = useCallback(() => {
    setSkillsChestLidOpen(true);
    dispatch(setInteractionPoint(generateRandomInteractionPoint()));
  }, [dispatch]);

  const closeChest = useCallback(() => {
    dispatch(setCardContent({ logo: "", title: undefined, text: "" }));
    dispatch(setInteractionPoint(generateRandomInteractionPoint()));
    setSkillsChestLidOpen(false);
  }, [dispatch]);

  useEffect(() => {
    if (skillsChestLidOpen && intersectionRatio < 0.3) {
      closeChest();
    }
  }, [closeChest, intersectionRatio, skillsChestLidOpen]);

  const contentToShow = useMemo(() => {
    return titleToShow ?
      <Box sx={horizontalContentWrapperSx}>
        <div style={partitionCss}>
          <img src={logoToShow} style={logoCss} alt="React logo" />
        </div>
        <div style={verticalLineCss} />
        <Typography sx={partitionCss} variant={"h6"} align={"center"}>
          {textToShow}
        </Typography>
      </Box> : startingContent;
  }, [logoToShow, textToShow, titleToShow]);

  return (
    <Box id={"skillsChestCore"} sx={skillsChestCoreSx}>
      <Box id={"skillsChestCircleBase"} sx={skillsChestCircleBaseSx} />
      <Box id={"bottomFace"} sx={skillsChestBottomFaceSx}>
        <DivSkillsChestCards />
      </Box>
      <Box id={"innerBackFaceA"} sx={skillsChestInnerBackFaceSx}>
        <Box id={"innerBackFaceB"} sx={skillsChestBasicFaceBDarkSx} />
      </Box>
      <Box id={"innerRightFace"} sx={skillsChestInnerRightFaceSx}>
        <Box id={"innerRightFaceB"} sx={skillsChestBasicFaceBDarkSx} />
      </Box>
      <Box id={"outerTopFace"} sx={skillsChestLidOpen ? skillsChestTopFacePointerEventsNoneSx : skillsChestTopFaceSx} />
      <Box id={"outerFrontFaceA"} sx={skillsChestOuterFrontFaceASx} ref={buttonContainerRef}>
        <Box id={"outerFrontFaceB"} sx={skillsChestBasicFaceBBrightSx} />
        <Slide in={!skillsChestLidOpen} direction={"left"} container={buttonContainerRef.current}>
          <Button size={"small"} color={"success"} variant={"text"} sx={buttonSx} onClick={openChest}>
            OPEN
          </Button>
        </Slide>
        <Slide in={skillsChestLidOpen} direction={"left"} container={buttonContainerRef.current}>
          <Button size={"small"} color={"error"} variant={"text"} sx={buttonSx} onClick={closeChest}>
            CLOSE
          </Button>
        </Slide>
      </Box>
      <Box id={"outerLeftFaceA"} sx={skillsChestOuterLeftFaceSx}>
        <Box id={"outerLeftFaceB"} sx={skillsChestBasicFaceBDarkSx} />
      </Box>
      <Box id={"outerRightFace"} sx={skillsChestOuterRightFaceSx} />
      <Box id={"outerBackFace"} sx={skillsChestOuterBackFaceSx} />
      <Box id={"chestLidFrontA"} sx={skillsChestLidOpen ? skillsChestLidFrontOpenedSx : skillsChestLidFrontClosedSx}>
        <Box id={"chestLidFrontB"} sx={skillsChestBasicFaceBBrightSx}>
          <Box sx={headerWrapperSx}>
            <Typography variant={"h6"} align={"center"}>{titleToShow}</Typography>
          </Box>
          {contentToShow}
          <Box sx={arrowsAndTitlesWrapperSx}>
            <Box sx={arrowTitlesWrapperSx}>
              <Typography variant={"h6"}>
                Languges
              </Typography>
              <Typography variant={"h6"}>
                Technologies
              </Typography>
              <Typography variant={"h6"}>
                Areas
              </Typography>
            </Box>
            <Box sx={arrowsWrapperSx}>
              <ArrowDropDownIcon sx={arrowSx} />
              <ArrowDropDownIcon sx={arrowSx} />
              <ArrowDropDownIcon sx={arrowSx} />
            </Box>
          </Box>
        </Box>
        <Box id={"chestLidBackA"} sx={skillsChestLidBackSx}>
          <Box id={"chestLidBackB"} sx={skillsChestBackLidSx} />
        </Box>
      </Box>
    </Box>
  );
}

export default DivSkillsChest;
