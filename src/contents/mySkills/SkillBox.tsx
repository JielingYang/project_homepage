import { CSSProperties, useMemo } from "react";
import { SKILL_BOX_HEIGHT_IN_VW, SKILL_BOX_WIDTH_IN_VW, SKILL_BOXES_GAP_IN_VW } from "../contentsUtils.ts";
import { isEvenNumber } from "../../UTIL.ts";
import { XLARGE_PADDING_IN_VW } from "../../UI_CONST.ts";
import { TSkillSetContent } from "../../TYPE.ts";
import { Box, SxProps } from "@mui/material";

const rootBasicCss: CSSProperties = {
    position: 'absolute',
    boxSizing: 'border-box',
    width: `${SKILL_BOX_WIDTH_IN_VW}vw`,
    height: `${SKILL_BOX_HEIGHT_IN_VW}vw`,
    // backgroundColor: 'red',
    display: 'flex',
    clipPath: 'polygon(0% 10%, 100% 0%, 100% 90%, 0 100%)',
    transition: 'top 1s',
    pointerEvents: 'none',
}

const skillBoxBackgroundBasicCss: CSSProperties = {
    position: 'absolute',
    boxSizing: 'border-box',
    width: '100%',
    height: '100%',
    margin: 'auto',
    display: 'flex',
    clipPath: 'polygon(0% 10%, 100% 0%, 100% 90%, 0 100%)',
}

const skillBoxImagesBasicCss: CSSProperties = {
    position: 'relative',
    boxSizing: 'border-box',
    margin: 'auto',
    width: '3vw',
    height: '3vw',
}

const skillBoxImagesFilterSx: SxProps = {
    position: 'absolute',
    boxSizing: 'border-box',
    width: '100%',
    height: '100%',
    backdropFilter: 'brightness(0.7)',
    clipPath: 'polygon(0% 10%, 100% 0%, 100% 90%, 0 100%)',
    pointerEvents: 'auto',
    transition: 'backdrop-filter 0.3s',
    '&:hover': {
        backdropFilter: 'brightness(1)',
    },
}

interface ISkillBox {
    content: TSkillSetContent
    boxIndex: number
    shouldAlign: boolean
    contentDirection: 'left' | 'right'
}

function SkillBox({ contentDirection, content, boxIndex, shouldAlign }: ISkillBox) {
    const rootCss: CSSProperties = useMemo(
        () => ({
            ...rootBasicCss,
            left: contentDirection === 'left' ? `${boxIndex * (SKILL_BOX_WIDTH_IN_VW + SKILL_BOXES_GAP_IN_VW)}vw` : 'auto',
            right: contentDirection === 'left' ? 'auto' : `${boxIndex * (SKILL_BOX_WIDTH_IN_VW + SKILL_BOXES_GAP_IN_VW)}vw`,
            top: shouldAlign ? 0 : `${isEvenNumber(boxIndex) ? -XLARGE_PADDING_IN_VW : XLARGE_PADDING_IN_VW}vw`,
        }),
        [boxIndex, contentDirection, shouldAlign]
    )

    const skillBoxBackgroundCss: CSSProperties = useMemo(
        () => ({
            ...skillBoxBackgroundBasicCss,
            backgroundColor: content.backgroundColor,
        }),
        [content.backgroundColor]
    )

    return (
        <div style={rootCss}>
            <div style={skillBoxBackgroundCss}>
                <img src={content.imgSrc} style={skillBoxImagesBasicCss} alt="React logo" />
                <Box sx={skillBoxImagesFilterSx} />
            </div>
        </div>
    )
}

export default SkillBox
