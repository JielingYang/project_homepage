import { CSSProperties, useEffect, useMemo, useRef, useState } from "react";
import { INTERSECT_ENTRY_THRESHOLD_1, INTERSECT_ENTRY_THRESHOLD_2, XXLARGE_PADDING_IN_VW } from "../../UI_CONST.ts";
import SkillBox from "./SkillBox.tsx";
import { SKILL_BOX_HEIGHT_IN_VW, SKILL_SET_1_CONTENTS } from "../contentsUtils.ts";
import { useIntersectEntry } from "../../hooks/hooks.ts";
import { Grow, Slide, Typography } from "@mui/material";

const rootCss: CSSProperties = {
    boxSizing: 'border-box',
    position: 'relative',
    // border: '1px solid rgba(255,255,255,0.1)',
    width: '90vw',
    height: `${SKILL_BOX_HEIGHT_IN_VW}vw`,
    margin: `${XXLARGE_PADDING_IN_VW}vw auto`,
    rowGap: `${XXLARGE_PADDING_IN_VW}vw`,
}

const textContainerBasicCss: CSSProperties = {
    position: 'absolute',
    boxSizing: 'border-box',
    // border: '1px solid rgba(255,255,255,0.05)',
    width: '50%',
    height: '100%',
    display: 'flex',
    top: 0,
}

interface ISkillBoxesContainer {
    contentDirection: 'left' | 'right'
}

function SkillBoxesContainer({ contentDirection }: ISkillBoxesContainer) {
    const ref = useRef(null)
    const intersectEntry = useIntersectEntry(ref)

    const [intersectThresholdTrigger1, setIntersectThresholdTrigger1] = useState(false)
    const [intersectThresholdTrigger2, setIntersectThresholdTrigger2] = useState(false)

    useEffect(() => {
        if (intersectEntry) {
            const trigger1 = intersectEntry.intersectionRatio >= INTERSECT_ENTRY_THRESHOLD_1
            if (trigger1 !== intersectThresholdTrigger1) {
                setIntersectThresholdTrigger1(trigger1)
            }
            const trigger2 = intersectEntry.intersectionRatio >= INTERSECT_ENTRY_THRESHOLD_2
            if (trigger2 !== intersectThresholdTrigger2) {
                setIntersectThresholdTrigger2(trigger2)
            }
        }
    }, [intersectEntry, intersectThresholdTrigger1, intersectThresholdTrigger2])

    const textContainerCss: CSSProperties = useMemo(
        () => ({
            ...textContainerBasicCss,
            left: contentDirection === 'left' ? 'auto' : 0,
            right: contentDirection === 'left' ? 0 : 'auto',
        }),
        [contentDirection]
    )

    const skillBoxes = useMemo(() => {
        return SKILL_SET_1_CONTENTS.map((content, i) => (
            <SkillBox contentDirection={contentDirection} key={i} boxIndex={i} shouldAlign={intersectThresholdTrigger2} content={content} />
        ))
    }, [contentDirection, intersectThresholdTrigger2])

    return (
        <Grow in={intersectThresholdTrigger1} timeout={500}>
            <div ref={ref} style={rootCss}>
                {skillBoxes}
                {/*<Fade in={intersectThresholdTrigger2} timeout={500}>*/}
                <Slide direction={contentDirection} in={intersectThresholdTrigger1} timeout={500}>
                    <div style={textContainerCss}>
                        <Typography variant="body1" sx={{ margin: 'auto' }}>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, voluptatum. Quisquam, voluptatum.
                        </Typography>
                    </div>
                </Slide>
                {/*</Fade>*/}
            </div>
        </Grow>
    )
}

export default SkillBoxesContainer
