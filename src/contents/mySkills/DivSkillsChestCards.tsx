import { Box, SxProps } from "@mui/material";
import DivSkillsChestCard from "./DivSkillsChestCard.tsx";
import { CHEST_HEIGHT_IN_VW } from "../../UI_CONST.ts";
import reactLogo from "../../assets/react.svg";
import babylonLogo from "../../assets/babylonLogo.svg";
import cssLogo from "../../assets/cssLogo.svg";
import springLogo from "../../assets/springLogo.svg";
import muiLogo from "../../assets/muiLogo.svg";
import svgLogo from "../../assets/svgLogo.svg";
import tsLogo from "../../assets/ts.svg";
import godotLogo from "../../assets/godotLogo.svg";
import reduxLogo from "../../assets/reduxLogo.svg";
import unityLogo from "../../assets/unityLogo.svg";
import javaLogo from "../../assets/javaLogo.svg";
import jsLogo from "../../assets/jsLogo.svg";
import cSharpLogo from "../../assets/cSharpLogo.svg";
import uiuxLogo from "../../assets/uiuxLogo.svg";
import securityLogo from "../../assets/securityLogo.svg";
import devOpsLogo from "../../assets/devOpsLogo.svg";
import codingLogo from "../../assets/codingLogo.svg";

const divSkillsChestCardsRootSx: SxProps = {
  position: "absolute",
  left: 0,
  top: 0,
  boxSizing: "border-box",
  width: "100%",
  height: "100%",
  transformStyle: "preserve-3d",
  transform: `translateZ(${CHEST_HEIGHT_IN_VW * 0.5}vw) translateY(-6%)`,
  display: "flex",
  flexDirection: "column",
  flexWrap: "wrap",
  alignItems: "center"
};

function DivSkillsChestCards() {

  return (
    <Box sx={divSkillsChestCardsRootSx} id={"divSkillsChestCardsRoot"}>
      <DivSkillsChestCard disabled={false} title={"JavaScript"} backgroundColor={"#323330"} borderColor={"#af9c39"} titleColor={"#f0db4f"} logo={jsLogo} text={"Who doesn't know JavaScript?"} />
      <DivSkillsChestCard disabled={false} title={"Java"} backgroundColor={"#ffffff"} borderColor={"#3174b9"} titleColor={"#3174b9"} logo={javaLogo} text={"My very first programming language..."} />
      <DivSkillsChestCard disabled={false} title={"C#"} backgroundColor={"#d8d3e1"} borderColor={"#a179dc"} titleColor={"#390091"} logo={cSharpLogo} text={"I use it for Unity"} />
      <DivSkillsChestCard />
      <DivSkillsChestCard />
      <DivSkillsChestCard />
      <DivSkillsChestCard />
      <DivSkillsChestCard />
      <DivSkillsChestCard />
      <DivSkillsChestCard />
      <DivSkillsChestCard />
      <DivSkillsChestCard />
      <DivSkillsChestCard disabled={false} title={"React"} backgroundColor={"rgb(40,40,40)"} borderColor={"rgb(0,98,114)"} titleColor={"rgb(0,222,222)"} logo={reactLogo} text={"I've been using React for years, from good old life-cycle functions to hooks today"} />
      <DivSkillsChestCard disabled={false} title={"Unity"} backgroundColor={"#d7d7d7"} borderColor={"#737373"} titleColor={"#000000"} logo={unityLogo} text={"I enjoy creating games, Unity is one of the tool I use"} />
      <DivSkillsChestCard disabled={false} title={"CSS"} backgroundColor={"#201936"} borderColor={"#1b73ba"} titleColor={"#ffffff"} logo={cssLogo} text={"CSS can satisfy most of my customisation requirements"} />
      <DivSkillsChestCard disabled={false} title={"spring"} backgroundColor={"#f1f1f1"} borderColor={"#70AD51"} titleColor={"#70AD51"} logo={springLogo} text={"Spring = backend"} />
      <DivSkillsChestCard disabled={false} title={"TypeScript"} backgroundColor={"#3178c6"} borderColor={"#cecece"} titleColor={"#ffffff"} logo={tsLogo} text={"Can't live without TypeScript..."} />
      <DivSkillsChestCard />
      <DivSkillsChestCard disabled={false} title={"MUI"} backgroundColor={"#f1f1f1"} borderColor={"#007FFF"} titleColor={"#007FFF"} logo={muiLogo} text={"MUI is quite useful in my daily work for common UI components!"} />
      <DivSkillsChestCard disabled={false} title={"Redux"} backgroundColor={"#242526"} borderColor={"#764ABC"} titleColor={"#ba8fff"} logo={reduxLogo} text={"Handy if I need complex state management"} />
      <DivSkillsChestCard disabled={false} title={"GODOT"} backgroundColor={"#e0edff"} borderColor={"#478cbf"} titleColor={"#478cbf"} logo={godotLogo} text={"Another great tool for creating games for me"} />
      <DivSkillsChestCard disabled={false} title={"babylon.js"} backgroundColor={"#201936"} borderColor={"#bb464b"} titleColor={"#e0ded8"} logo={babylonLogo} text={"To me, BabylonJS is the man for the job if I need to render 3D graphics in browser"} />
      <DivSkillsChestCard disabled={false} title={"SVG"} backgroundColor={"#333333"} borderColor={"#ce8f31"} titleColor={"#ffffff"} logo={svgLogo} text={"When requirements become fancy, SVG could save my day"} />
      <DivSkillsChestCard />
      <DivSkillsChestCard />
      <DivSkillsChestCard />
      <DivSkillsChestCard />
      <DivSkillsChestCard />
      <DivSkillsChestCard />
      <DivSkillsChestCard />
      <DivSkillsChestCard disabled={false} title={"UI/UX"} backgroundColor={"rgb(0,58,66)"} borderColor={"rgb(0,164,150)"} titleColor={"rgb(0,203,180)"} logo={uiuxLogo} />
      <DivSkillsChestCard disabled={false} title={"Security"} backgroundColor={"rgb(0,58,66)"} borderColor={"rgb(0,164,150)"} titleColor={"rgb(0,203,180)"} logo={securityLogo} />
      <DivSkillsChestCard disabled={false} title={"DevOps"} backgroundColor={"rgb(0,58,66)"} borderColor={"rgb(0,164,150)"} titleColor={"rgb(0,203,180)"} logo={devOpsLogo} />
      <DivSkillsChestCard disabled={false} title={"Development"} backgroundColor={"rgb(0,58,66)"} borderColor={"rgb(0,164,150)"} titleColor={"rgb(0,203,180)"} logo={codingLogo} />
      <DivSkillsChestCard />
      <DivSkillsChestCard />
    </Box>
  );
}

export default DivSkillsChestCards;
