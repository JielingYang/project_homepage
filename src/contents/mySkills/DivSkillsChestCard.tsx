import { Box, SxProps } from "@mui/material";
import { CHEST_HEIGHT_IN_VW } from "../../UI_CONST.ts";
import { CSSProperties, useCallback, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setCardContent } from "../../redux/reducers/skillsChestStateReducer.ts";
import { TRootState } from "../../redux/store.ts";

const defaultBackgroundColor = "rgb(30,38,37)";

const divSkillsChestCardRootSx: SxProps = {
  position: "relative",
  boxSizing: "border-box",
  width: "16%",
  height: `${CHEST_HEIGHT_IN_VW * 0.8}vw`,
  transform: "rotateX(-90deg)",
  transformStyle: "preserve-3d",
  backfaceVisibility: "hidden",
  borderRadius: "0.2vw",
  boxShadow: "0 0 0.55vw 0.1vw rgba(0,0,0,0.6)",
  transition: "transform 0.3s ease-in-out, box-shadow 0.3s ease-in-out, filter 0.3s",
  pointerEvents: "auto",
  cursor: "pointer",
  filter: "brightness(0.7)",
  "&:hover": {
    boxShadow: "0 0 0.4vw 0.2vw rgba(0,0,0,0.9)",
    zIndex: 1,
    filter: "brightness(1)"
  },
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  padding: "0.1vw",
  fontFamily: "system-ui",
  fontSize: "0.7vw",
  border: "0.2vw solid rgba(0,0,0,0)"
};

const titleSx: SxProps = {
  width: "100%",
  textAlign: "center",
  borderRadius: "0.1vw",
  fontWeight: "bold"
};

const logoCss: CSSProperties = {
  position: "relative",
  boxSizing: "border-box",
  margin: "auto",
  width: "3vw",
  height: "3vw"
};


interface IDivSkillsChestCard {
  disabled?: boolean;
  title?: string;
  titleColor?: string;
  backgroundColor?: string;
  borderColor?: string;
  logo?: string;
  text?: string;
}

function DivSkillsChestCard({ title, logo, backgroundColor, borderColor, titleColor, disabled = true, text = "" }: IDivSkillsChestCard) {
  const dispatch = useDispatch();
  const titleToShow = useSelector((state: TRootState) => state.skillsChestState.titleToShow);

  const isCardSelected = useMemo(() => !!title && titleToShow === title, [titleToShow, title]);

  const handleMouseClick = useCallback(() => {
    if (!isCardSelected) {
      dispatch(setCardContent({ logo: logo ? logo : "", title: title ? title : "", text: text }));
    } else {
      dispatch(setCardContent({ logo: "", title: undefined, text: text }));
    }
  }, [dispatch, isCardSelected, logo, text, title]);


  return (
    <Box onClick={handleMouseClick} sx={{
      ...divSkillsChestCardRootSx,
      borderColor: borderColor ? borderColor : "none",
      backgroundColor: backgroundColor ? backgroundColor : defaultBackgroundColor,
      pointerEvents: disabled ? "none" : "auto",
      transform: isCardSelected ? "rotateX(-90deg) translateY(-65%)" : "rotateX(-90deg)"
    }}
         id={"divSkillsChestCard"}>
      {title ? <Box sx={{ ...titleSx, color: titleColor ? titleColor : "white" }} id={"title"}>{title}</Box> : <></>}
      {logo ? <img src={logo} style={logoCss} alt="React logo" /> : <></>}
    </Box>
  );
}

export default DivSkillsChestCard;
