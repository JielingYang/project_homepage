import { CSSProperties, useRef } from "react";
import { useIntersectRatio } from "../../hooks/hooks.ts";
import { DEFAULT_BORDER_RADIUS, HEAVY_BLUR, XLARGE_PADDING_IN_VW, XXLARGE_PADDING_IN_VW } from "../../UI_CONST.ts";
import DivSkillsChest from "./DivSkillsChest.tsx";
import { Grow, Typography } from "@mui/material";

const panelHeightInVh = 90;

const rootCss: CSSProperties = {
    position: "relative",
    boxSizing: "border-box",
    width: "100%",
    height: `${panelHeightInVh}vh`,
    display: "flex"
};

const panelBackgroundCss: CSSProperties = {
    boxSizing: 'border-box',
    position: 'absolute',
    width: '90vw',
    height: `${panelHeightInVh}vh`,
    minWidth: '921px',
    minHeight: '409px',
    backgroundColor: 'rgba(0,0,0,0.2)',
    borderRadius: DEFAULT_BORDER_RADIUS,
    left: '50%',
    top: '50%',
    transform: 'translate(-50%,-50%)',
}

const panelBlurCss: CSSProperties = {
    position: 'absolute',
    width: '100%',
    height: '100%',
    borderRadius: DEFAULT_BORDER_RADIUS,
    backdropFilter: HEAVY_BLUR,
}

const panelTextContainerCss: CSSProperties = {
    boxSizing: 'border-box',
    position: 'absolute',
    width: `${40}vw`,
    height: '100%',
    right: 0,
    top: 0,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    rowGap: `${XLARGE_PADDING_IN_VW}vw`,
    padding: `${XXLARGE_PADDING_IN_VW}vw`,
    textAlign: 'center',
}

const perspectivePanelCss: CSSProperties = {
    ...panelBackgroundCss,
    overflow: 'visible',
    backgroundColor: 'none',
    perspectiveOrigin: '50% 10%',
    perspective: '2500px',
    transformStyle: 'preserve-3d',
}

const displayNoneCss: CSSProperties = {
    display: 'none',
}

// @ts-ignore
const MySkills = (() => {
    const ref = useRef<HTMLDivElement>(null);
    const panelIntersectRatio = useIntersectRatio(ref)

    // console.log('MySkills', panelIntersectRatio)
    return (
        <div id={'mySkills'} ref={ref} style={rootCss}>
            <div style={panelIntersectRatio === 0 ? displayNoneCss : panelBackgroundCss}>
                <div style={panelBlurCss} />
                <div style={panelTextContainerCss}>
                    <Grow in={panelIntersectRatio > 0.7} timeout={1000}>
                        <Typography variant={'body2'}>
                            {"Come open the box and see what's inside!"}
                        </Typography>
                    </Grow>
                </div>
            </div>
            <div style={perspectivePanelCss}>
                <DivSkillsChest intersectionRatio={panelIntersectRatio} />
            </div>
        </div>
    )
})

export default MySkills
