import { TSkillSetContent } from '../TYPE'
import tsLogo from '../assets/ts.svg'
import muiLogo from '../assets/muiLogo.svg'
import reactLogo from '../assets/react.svg'
import jsLogo from '../assets/jsLogo.svg'
import reduxLogo from '../assets/reduxLogo.svg'
import babylonLogo from '../assets/babylonLogo.svg'
import svgLogo from '../assets/svgLogo.svg'
import cssLogo from '../assets/cssLogo.svg'
import javaLogo from '../assets/javaLogo.svg'

export const SKILL_BOX_WIDTH_IN_VW = 4
export const SKILL_BOX_HEIGHT_IN_VW = 35
export const SKILL_BOXES_GAP_IN_VW = 0.5

export const SKILL_SET_1_CONTENTS: Array<TSkillSetContent> = [
    { backgroundColor: '#3178c6', imgSrc: tsLogo },
    { backgroundColor: '#ffffff', imgSrc: muiLogo },
    { backgroundColor: 'rgb(35,39,47)', imgSrc: reactLogo },
    { backgroundColor: '#323330', imgSrc: jsLogo },
    { backgroundColor: '#efe0ff', imgSrc: reduxLogo },
    { backgroundColor: 'rgb(54,25,25)', imgSrc: babylonLogo },
    { backgroundColor: 'rgb(247,148,0)', imgSrc: svgLogo },
    { backgroundColor: '#afc5c5', imgSrc: cssLogo },
    { backgroundColor: '#ffffff', imgSrc: javaLogo },
]

const generateIntersectThresholdsArray = () => {
    const thresholds = []
    for (let i = 1; i <= 100; i++) {
        thresholds.push(Number((i * 0.01).toFixed(2)))
    }
    return thresholds
}

export const INTERSECT_ENTRY_THRESHOLDS = generateIntersectThresholdsArray()

export const CUBE_SIZE_IN_VW = 13
export const CUBE_LEFT_IN_VW = 60
