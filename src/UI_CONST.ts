export const DEFAULT_BOX_SHADOW = '0px 0px 0.3vw 0.15vw rgba(0,0,0,0.4)'

export const DEFAULT_BLUR = 'blur(5px)'
export const HEAVY_BLUR = 'blur(10px)'

export const DEFAULT_PADDING_IN_PX = 10
export const LARGE_PADDING_IN_PX = 20
export const XLARGE_PADDING_IN_PX = 40

export const DEFAULT_PADDING_IN_VW = 0.75
export const LARGE_PADDING_IN_VW = 1.5
export const XLARGE_PADDING_IN_VW = 3
export const XXLARGE_PADDING_IN_VW = 6

export const DEFAULT_BORDER_RADIUS = '5px'

export const INTERSECT_ENTRY_THRESHOLD_1 = 0.5
export const INTERSECT_ENTRY_THRESHOLD_2 = 1

export const CHEST_HEIGHT_IN_VW = 6

export const FPS60 = 60;
export const EVENT_TIME_GAP = (1 / FPS60) * 1000; // Multiply by 1000 to get ms

