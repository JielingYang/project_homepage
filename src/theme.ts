import { createTheme } from "@mui/material";

declare module '@mui/material/styles' {
    interface Theme {
        specials: {
            shadow: string
            insetShadow: string
        }
    }

    interface ThemeOptions {
        specials: {
            shadow: string
            insetShadow: string
        }
    }
}

export const theme = createTheme({
    typography: {
        // fontFamily: 'Arial Black',
        body1: {
            fontSize: 'clamp(18px, 1.8vw, 35px)',
            textTransform: 'none',
            color: 'rgb(162,162,162)',
            textShadow: '0 1px 1vw rgba(0, 0, 0, 0.9)',
            pointerEvents: 'none',
        },
        body2: {
            fontSize: 'clamp(18px, 1.8vw, 35px)',
            textTransform: 'none',
            background: 'linear-gradient(135deg, #8B8BABFF, #9D8974FF 70%)',
            backgroundClip: 'text',
            textFillColor: 'transparent',
            pointerEvents: 'none',
        },
        h1: {
            fontSize: 'clamp(25px, 2.5vw, 45px)',
            textAlign: 'center',
            background: 'linear-gradient(rgb(150,150,155) 40%, rgb(100,100,100))',
            backgroundClip: 'text',
            textFillColor: 'transparent',
            pointerEvents: 'none',
        },
        h6: {
            fontSize: '1vw',
            textTransform: 'none',
            color: '#4f4f4f',
            textShadow: '0 0 0.1vw rgb(0, 0, 0, 0.1)',
        },
    },
    palette: {
        primary: {
            light: '#ffe36f',
            main: '#fbb900',
            contrastText: '#565656',
        },
        secondary: {
            light: '#ededed',
            main: '#e2e2e2',
            dark: '#cccccc',
        },
        success: {
            light: '#d3ffba',
            main: '#b7e49d',
            dark: '#457928',
        },
        error: {
            light: '#f8ced0',
            main: '#f75c5c',
            dark: '#951e1e',
        },
    },
    specials: {
        shadow: '0 1px 3px 0px rgba(0, 0, 0, 1)',
        insetShadow: '0 1px 3px 0px rgba(0, 0, 0, 1) inset',
    },
})
