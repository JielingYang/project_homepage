import { Color3, Color4 } from "@babylonjs/core";
import { Vector3 } from "@babylonjs/core/Maths/math.vector";

export const enum CameraLayer {
  // MAIN_CAMERA_LAYER = parseInt("0xFFFFFFF"),
  MAIN_CAMERA_LAYER = 1,
  UI_CAMERA_LAYER = 2,
}
export const SHADOW_LIGHT_POSITION = new Vector3(10, 20, 10);
export const DIRECTIONAL_LIGHT01_VECTOR3 = new Vector3(-10, -20, -10);
export const DIRECTIONAL_LIGHT02_VECTOR3 = new Vector3(1.5, -0.1, 1);

export const GROUND_SIZE = 20;
export const DEFAULT_DOF_FOCAL_LENGTH = 100;
export const DEFAULT_DOF_F_STOP = 32;
export const DEFAULT_DOF_DISTANCE = 0;
export const MAX_DOF_LENS_SIZE = 50;
export const DOF_LENS_SIZE_CHANGE = 1;

// Camera
export const CAMERA_DEFAULT_ALPHA = -Math.PI / 3;
export const CAMERA_DEFAULT_BETA = Math.PI / 3;
export const CAMERA_DEFAULT_RADIUS = 30;
export const CAMERA_PANNING_AXIS = new Vector3(1, 0, 1);
export const CAMERA_UPPER_BETA_LIMIT = Math.PI / 2;
export const CAMERA_LOWER_BETA_LIMIT = 0;
export const CAMERA_UPPER_RADIUS_LIMIT = 100;
export const CAMERA_LOWER_RADIUS_LIMIT = 4;
export const CAMERA_WHEEL_PRECISION = 30;
export const CAMERA_PANNING_INERTIA = 0.5;
export const CAMERA_PANNING_SENSIBILITY = 50;

export const OBSTACLE_PREVIEW_BASE_MATERIAL_ALPHA = 0.3;
export const PRIMARY_YELLOW = Color3.FromInts(251, 185, 0);
export const GRAY10 = new Color3(0.1, 0.1, 0.1);
export const GRAY20 = new Color3(0.2, 0.2, 0.2);
export const GRAY25 = new Color3(0.25, 0.25, 0.25);
export const GRAY30 = new Color3(0.3, 0.3, 0.3);
export const GRAY35 = new Color3(0.35, 0.35, 0.35);
export const GRAY40 = new Color3(0.4, 0.4, 0.4);
export const GRAY50 = new Color3(0.5, 0.5, 0.5);
export const GRAY60 = new Color3(0.6, 0.6, 0.6);
export const GRAY70 = new Color3(0.7, 0.7, 0.7);
export const GRAY80 = new Color3(0.8, 0.8, 0.8);
export const GRAY90 = new Color3(0.9, 0.9, 0.9);
export const TRANSPARENT = new Color4(0, 0, 0, 0);
export const GRAY20_100 = new Color4(0.2, 0.2, 0.2, 1);
export const GRAY30_100 = new Color4(0.3, 0.3, 0.3, 1);
export const GRAY60_100 = new Color4(0.6, 0.6, 0.6, 1);
