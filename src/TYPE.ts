import {Property} from "csstype";

export type TPoint3 = {
    x: number
    y: number
    z: number
}

export type TPoint2 = {
    x: number
    y: number
}

export type TPoint3WithCoordLines = {
    id: number
    value: TPoint3
}

export type TSkillSetContent = {
    backgroundColor: Property.BackgroundColor
    imgSrc: string
}
