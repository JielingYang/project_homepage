export const SPACE_STRING = ' '

// Object names
export const COORD_1 = 'coord1'
export const OBJECT_PLACEMENT_GHOST = 'objectPlacementGhost'
export const UI_TEXTURE_NAME = 'ui'
export const HIGHLIGHT_LAYER = 'highlightLayer'
export const GLOW_LAYER = 'glowLayer'
export const CUSTOM_AXES_NAME = 'customAxes'
export const MAIN_CAMERA_NAME = 'mainCamera'
export const UI_CAMERA_NAME = 'uiCamera'
export const GROUND_MESH_NAME = 'groundMesh'
export const GROUND_MESH_HEIGHT_MAP_NAME = 'groundMeshHeightMap'
export const GENERAL_SHADOW_LIGHT_NAME = 'generalShadowLight'
export const HEMISPHERIC_LIGHT_NAME = 'hemisphericLight'
export const DIRECTIONAL_LIGHT01_NAME = 'directionalLight01'
export const DIRECTIONAL_LIGHT02_NAME = 'directionalLight02'
export const OBSTACLE_NAME = 'obstacle'
export const OBSTACLE_PREVIEW_BASE_MESH_NAME = 'obstaclePreviewBase'
export const OBSTACLE_PREVIEW_MESH_NAME = 'obstaclePreview'
export const OBSTACLE_DYNAMIC_PREVIEW_MESH_NAME = 'obstacleDynamicPreview'
export const CONVEYOR_PREVIEW_MESH_NAME = 'conveyorPreview'
export const ORIGINAL_CONVEYOR_MESH_NAME = 'originalConveyor'
export const CONVEYOR_MESH_NAME = 'conveyor'
export const LIGHT_CURTAIN_PREVIEW_MESH_PAIR_TRANSFORM_NODE_NAME = 'lightCurtainPreviewMeshPairTransformNode'
export const LIGHT_CURTAIN_PREVIEW_MESH_PAIR_SNAP_DETECTION_MESH_NAME = 'lightCurtainPreviewMeshSnapDetectionMesh'
export const DEFAULT_RENDERING_PIPELINE_NAME = 'defaultRenderingPipeline'
export const LENS_RENDERING_PIPELINE_NAME = 'lensRenderingPipeline'
export const TEMP_MESH_NAME = 'tempMesh'
export const MACHINE_MESH_NAME = 'machine'
export const RADAR_MESH_NAME = 'radar'
export const RADAR_RANGE_MESH_NAME = 'radarRange'

// Material names
export const INVISIBLE_MATERIAL_NAME = 'invisibleMaterial'
export const WORLD_AXIS_MATERIAL_NAME = 'worldAxisMaterial'
export const GROUND_MESH_MATERIAL_NAME = 'groundMaterial'
export const OBSTACLE_PREVIEW_GRAY_MATERIAL_NAME = 'obstaclePreviewGrayMaterial'
export const OBSTACLE_PREVIEW_YELLOW_MATERIAL_NAME = 'obstaclePreviewYellowMaterial'
export const OBSTACLE_PREVIEW_GREEN_MATERIAL_NAME = 'obstaclePreviewGreenMaterial'
export const OBSTACLE_PREVIEW_RED_MATERIAL_NAME = 'obstaclePreviewRedMaterial'
export const OBSTACLE_PREVIEW_BASE_MATERIAL_NAME = 'obstaclePreviewBaseMaterial'
export const OBSTACLE_MATERIAL_NAME = 'obstacleMaterial'
export const CONVEYOR_BODY_MATERIAL_NAME = 'conveyorBodyMaterial'
export const RADAR_RANGE_MATERIAL_NAME = 'radarRangeMaterial'

// Texture names
export const GROUND_MESH_MIRROR_TEXTURE_NAME = 'groundMirrorTexture'

export const LIGHT_CURTAIN_BUTTON_NAME = 'LightCurtainButton'
export const MAT_BUTTON_NAME = 'MatButton'

// Multilines
export const LIGHT_CURTAIN_CONFIG_BEAM_CODING_LINE_NAME = 'lightCurtainConfig_beamCodingLine'

// miscellaneous
export const LIGHT_CURTAIN_PAIR = 'lightCurtainPair'
export const RECEIVER = 'receiver'
export const TRANSMITTER = 'transmitter'
