import { Vector3 } from '@babylonjs/core/Maths/math.vector'
import { TPoint3 } from './TYPE.ts'
import { BoundingBox, Mesh, Node, Scene } from '@babylonjs/core'
import { addTagsToTarget, ObjectTags } from './contents/prototype3dTool/helper/tag.ts'
import {
    setupPointerEnterMeshAction,
    setupPointerExitMeshAction,
    setupPointerPickMeshAction,
} from './contents/prototype3dTool/helper/selectionHelper.ts'
import { addVisualEffectToMesh } from './contents/prototype3dTool/helper/light.ts'

export const randomNumFromInterval = (min: number, max: number) => {
    return Math.random() * (max - min + 1) + min
}

export const isEvenNumber = (num: number): boolean => {
    return num % 2 === 0
}

export const vector3ToPoint3 = (point: Vector3): TPoint3 => {
    return { x: point.x, y: point.y, z: point.z }
}

export const vector3ArrayToPoint3Array = (array: Array<Vector3>): Array<TPoint3> => array.map((point) => vector3ToPoint3(point))

export const point3ToVector3 = (point: TPoint3): Vector3 => new Vector3(point.x, point.y, point.z)

export const point3ArrayToVector3Array = (array: Array<TPoint3>): Array<Vector3> => array.map((point) => point3ToVector3(point))

export const getAngleOfDirectionOnXZ = (direction: Vector3): number =>
    Vector3.GetAngleBetweenVectors(direction, Vector3.Forward(), Vector3.Down())

export const worldToLocal = (worldVector3: Vector3, localTarget: Node) => {
    const worldMatrixInverted = localTarget.getWorldMatrix().clone().invert()
    return Vector3.TransformCoordinates(worldVector3, worldMatrixInverted)
}

export const localToWorld = (worldVector3: Vector3, localTarget: Node) => {
    const worldMatrix = localTarget.getWorldMatrix()
    return Vector3.TransformCoordinates(worldVector3, worldMatrix)
}

export const getLocalBottom4PointsOfBoundingBox = (boundingBox: BoundingBox): Array<Vector3> => {
    const min = boundingBox.minimum
    const max = boundingBox.maximum
    const p1 = min.clone()
    const p2 = new Vector3(max.x, min.y, max.z)
    const p3 = new Vector3(min.x, min.y, max.z)
    const p4 = new Vector3(max.x, min.y, min.z)
    return [p1, p2, p3, p4]
}

export const processCreatedMesh = (
    scene: Scene,
    mesh: Mesh,
    tags: Array<ObjectTags>,
    addVisualEffect = true,
    pickable = true,
    metadata?: object
) => {
    addVisualEffect && addVisualEffectToMesh(scene, mesh)
    metadata && (mesh.metadata = metadata)
    addTagsToTarget(mesh, tags)

    setupPointerEnterMeshAction(scene, mesh)
    setupPointerExitMeshAction(scene, mesh)
    pickable && setupPointerPickMeshAction(scene, mesh)
}
