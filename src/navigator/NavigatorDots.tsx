import { CSSProperties, useEffect, useState } from 'react'
import { LARGE_PADDING_IN_PX } from '../UI_CONST'
import { dotsContent, NAVIGATOR_SHRUNK_WIDTH_IN_PX } from './navigatorUtils'
import NavigatorDot from './NavigatorDot'
import NavigatorConnector from './NavigatorConnector'

const navigatorDotsCss: CSSProperties = {
    boxSizing: 'border-box',
    position: 'relative',
    left: 0,
    top: 0,
    width: `${NAVIGATOR_SHRUNK_WIDTH_IN_PX}px`,
    height: '100%',
    boxShadow: 'none',
    paddingTop: `${LARGE_PADDING_IN_PX}px`,
    paddingBottom: `${LARGE_PADDING_IN_PX}px`,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
}

function NavigatorDots() {
    // const [activeDot, setActiveDot] = useState(0)
    const [dotsAndConnectors, setDotsAndConnectors] = useState<JSX.Element[]>([])

    // const dotClick = useCallback(
    //     (dotIndex: number) => () => {
    //         setActiveDot(dotIndex)
    //     },
    //     []
    // )

    useEffect(() => {
        const result: JSX.Element[] = []
        dotsContent.forEach((dotContent, index) => {
            result.push(<NavigatorDot dotLabel={dotContent} dotIndex={index} key={`navigatorDot_${index}`} />)
            result.push(<NavigatorConnector key={`navigatorConnector_${index}`} />)
        })
        result.pop()
        setDotsAndConnectors(result)
    }, [])

    console.log('navigatorDots')
    return (
        <div id={'navigatorDots'} style={navigatorDotsCss}>
            {dotsAndConnectors}
        </div>
    )
}

export default NavigatorDots
