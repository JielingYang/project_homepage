import { CSSProperties, useMemo } from 'react'
import { NAVIGATOR_DOT_SIZE_IN_PX, NAVIGATOR_EXPANDED_WIDTH_IN_PX, NAVIGATOR_SHRUNK_WIDTH_IN_PX } from './navigatorUtils'
import { SxProps, Typography } from '@mui/material'

const navigatorDotCss: CSSProperties = {
    position: 'relative',
    width: `${NAVIGATOR_DOT_SIZE_IN_PX}px`,
    height: `${NAVIGATOR_DOT_SIZE_IN_PX}px`,
    borderRadius: '50%',
    backgroundColor: 'rgba(255,255,255,0.5)',
    cursor: 'pointer',
    zIndex: 1,
}

const navigatorDotLabelBasicSx: SxProps = {
    position: 'absolute',
    width: `${NAVIGATOR_EXPANDED_WIDTH_IN_PX - NAVIGATOR_SHRUNK_WIDTH_IN_PX}px`,
    height: '20px',
    left: `calc(100% + ${(NAVIGATOR_SHRUNK_WIDTH_IN_PX - NAVIGATOR_DOT_SIZE_IN_PX) * 0.5}px)`,
    top: '50%',
    transform: 'translateY(-50%)',
    backgroundColor: 'rgba(255,255,255,0.5)',
    cursor: 'pointer',
    pointerEvents: 'none',
    opacity: 0,
    transition: 'opacity 0.5s',
}

interface INavigatorDot {
    dotIndex: number
    dotLabel: string
}

function NavigatorDot({ dotIndex, dotLabel }: INavigatorDot) {
    const navigatorDotLabelSx = useMemo(
        () => ({
            ...navigatorDotLabelBasicSx,
            transitionDelay: `${0.1 + dotIndex * 0.1}s`,
        }),
        [dotIndex]
    )

    return (
        <div id={`navigatorDot_${dotIndex}`} style={navigatorDotCss}>
            <Typography className={`navigatorDotLabel`} sx={navigatorDotLabelSx}>
                {dotLabel}
            </Typography>
        </div>
    )
}

export default NavigatorDot
