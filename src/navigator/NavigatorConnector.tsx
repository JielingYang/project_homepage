import { CSSProperties } from 'react'
import { DEFAULT_BOX_SHADOW } from '../UI_CONST'

const navigatorConnectorCss: CSSProperties = {
    position: 'relative',
    width: '0.5px',
    backgroundColor: 'rgba(255,255,255,0.3)',
    pointerEvents: 'none',
    flexGrow: 1,
    zIndex: 0,
    boxShadow: DEFAULT_BOX_SHADOW,
}

function NavigatorConnector() {
    return <div style={navigatorConnectorCss} />
}

export default NavigatorConnector
