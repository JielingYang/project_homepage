import { MutableRefObject, useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { DEFAULT_BOX_SHADOW, HEAVY_BLUR } from '../UI_CONST'
import { setInteractionPoint } from '../redux/reducers/interactiveTilesStateReducer'
import { generateRandomInteractionPoint } from '../interactiveTilesBackground/interactiveTilesBackgroundUtils'
import { NAVIGATOR_EXPANDED_WIDTH_IN_PX, NAVIGATOR_SHRUNK_WIDTH_IN_PX } from './navigatorUtils'
import NavigatorDots from './NavigatorDots'
import { Box, SxProps } from '@mui/material'

const navigatorSx: SxProps = {
    boxSizing: 'border-box',
    boxShadow: DEFAULT_BOX_SHADOW,
    position: 'fixed',
    width: `${NAVIGATOR_SHRUNK_WIDTH_IN_PX}px`,
    left: 0,
    top: 0,
    height: '100%',
    backdropFilter: HEAVY_BLUR,
    backgroundColor: 'rgba(255,255,255,0.05)',
    transition: 'width 0.5s',
    '&:hover': {
        width: `${NAVIGATOR_EXPANDED_WIDTH_IN_PX}px`,
        '& .navigatorDotLabel': {
            pointerEvents: 'auto',
            opacity: 1,
        },
    },
}

interface INavigator {
    contentsRefs: Array<MutableRefObject<HTMLDivElement>>
}

function Navigator({ contentsRefs }: INavigator) {
    // const ref = useSelector((state: TRootState) => state.refsState.skillsContainer1Ref)
    const dispatch = useDispatch()

    const triggerTiles = useCallback(() => {
        // noinspection TypeScriptValidateTypes
        dispatch(setInteractionPoint(generateRandomInteractionPoint()))
        contentsRefs[0]?.current?.scrollIntoView({ behavior: 'smooth' })
    }, [contentsRefs, dispatch])

    console.log('navigator')
    return (
        <Box id={'navigator'} sx={navigatorSx} onClick={triggerTiles}>
            <NavigatorDots />
        </Box>
    )
}

export default Navigator
