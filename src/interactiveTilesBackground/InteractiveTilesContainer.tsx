import { CSSProperties, ReactElement, useEffect } from "react";
import InteractiveTile from "./InteractiveTile";
import { generateRandomInteractionPoint, NUM_OF_TILES_PER_COLUMN, NUM_OF_TILES_PER_ROW } from "./interactiveTilesBackgroundUtils";
import { setInteractionPoint } from "../redux/reducers/interactiveTilesStateReducer";
import { useDispatch } from "react-redux";

const rootCss: CSSProperties = {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    perspective: '800px',
    boxShadow: 'inset 0 0 0.5vw 0.1vw rgba(0,0,0,0.5)',
    color: 'rgb(42,42,42)',
    background: 'linear-gradient(15deg, rgb(28,11,0) 0%, rgb(66,59,41) 50%, rgb(42,42,42) 100%)',
    overflow: 'hidden',
    zIndex: -1,
    pointerEvents: 'none',
}

function InteractiveTilesContainer() {
    const dispatch = useDispatch()

    useEffect(() => {
        if (document.readyState === 'complete' || document.readyState === 'interactive') {
            const timer = setTimeout(() => {
                dispatch(setInteractionPoint(generateRandomInteractionPoint()))
            }, 500)
            return () => clearTimeout(timer)
        }
    }, [dispatch])

    console.log('interactiveTilesContainer')
    return (
        <div id={'interactiveTilesContainer'} style={rootCss}>
            {generateTiles()}
        </div>
    )
}

const generateTiles = (): Array<ReactElement> => {
    const tiles: Array<ReactElement> = []
    for (let row = 0; row < NUM_OF_TILES_PER_ROW; row++) {
        for (let column = 0; column < NUM_OF_TILES_PER_COLUMN; column++) {
            tiles.push(<InteractiveTile key={`${row}*${column}`} idPostfix={`${row}*${column}`} row={row} column={column} />)
        }
    }
    return tiles
}

export default InteractiveTilesContainer
