import { randomNumFromInterval } from "../UTIL";

export const TILES_TRANSITION_TIME = 1.5 // seconds
export const NUM_OF_TILES_PER_ROW = 20
export const NUM_OF_TILES_PER_COLUMN = 20
export const TILES_DEFAULT_BORDER_RADIUS = 0;
export const TILES_DEFAULT_BORDER_WIDTH = 1;
export const INTERACTIVE_TILE_COLOR_RGBA = 'rgba(49,38,19,0.9)'

export const generateRandomInteractionPoint = () => {
    return { x: randomNumFromInterval(0, 2 * window.innerWidth), y: randomNumFromInterval(0, 2 * window.innerHeight) }
}
