import { CSSProperties, useEffect, useMemo, useRef, useState } from "react";
import { Box } from "@mui/material";
import { useSelector } from "react-redux";
import { TRootState } from "../redux/store";
import { NUM_OF_TILES_PER_COLUMN, NUM_OF_TILES_PER_ROW, TILES_TRANSITION_TIME } from "./interactiveTilesBackgroundUtils";
import { Nullable } from "@babylonjs/core";

const tileBasicCss: CSSProperties = {
    position: 'absolute',
    boxShadow: `0 0 1vw 0.1vw rgba(0,0,0,0.2)`,
    transformOrigin: '50% 50%',
    transform: 'rotateX(0) rotateY(0)',
    boxSizing: 'border-box',
    pointerEvents: 'none',
    willChange: 'transform',
    transition: `transform ${TILES_TRANSITION_TIME}s`,
    display: 'none',
    border: '1px solid rgba(0,0,0,0.2)',
}

interface IInteractiveTile {
    idPostfix: string
    row: number
    column: number
}

function InteractiveTile({ idPostfix, row, column }: IInteractiveTile) {
    const interactionPoint = useSelector((state: TRootState) => state.interactiveTilesState.interactionPoint)
    const tilesColor = useSelector((state: TRootState) => state.interactiveTilesState.tilesColor);
    const tilesBorderRadius = useSelector((state: TRootState) => state.interactiveTilesState.tilesBorderRadius);
    const tilesBorderWidth = useSelector((state: TRootState) => state.interactiveTilesState.tilesBorderWidth);
    const tilesBackfaceVisibility = useSelector((state: TRootState) => state.interactiveTilesState.tilesBackfaceVisibility);

    const tileRef = useRef<Nullable<HTMLDivElement>>(null)
    const [tileCss, setTileCss] = useState<CSSProperties>(tileBasicCss)

    const tileWidth = useMemo(() => `${100 / NUM_OF_TILES_PER_ROW}%`, [])
    const tileHeight = useMemo(() => `${100 / NUM_OF_TILES_PER_COLUMN}%`, [])
    const tileLeft = useMemo(() => `${(row * 100) / NUM_OF_TILES_PER_ROW}%`, [row])
    const tileTop = useMemo(() => `${(column * 100) / NUM_OF_TILES_PER_COLUMN}%`, [column])
    const tileBorderRadius = useMemo(() => `${(100 / NUM_OF_TILES_PER_COLUMN) * 0.1}%`, [])

    useEffect(() => {
        const target = tileRef.current

        if (target) {
            const windowWidth = window.innerWidth
            const windowHeight = window.innerHeight

            let tileTransform: string
            if (interactionPoint) {
                const tileCenterX = target.offsetLeft + target.offsetWidth * 0.5
                const tileCenterY = target.offsetTop + target.offsetHeight * 0.5
                const xDiff = interactionPoint.x - tileCenterX
                const yDiff = interactionPoint.y - tileCenterY

                const xRotation = 180 * (xDiff / windowWidth)
                const yRotation = 180 * (yDiff / windowHeight)
                tileTransform = `rotateX(${xRotation}deg) rotateY(${yRotation}deg)`
            } else {
                tileTransform = 'rotateX(0) rotateY(0)'
            }

            setTileCss({
                ...tileBasicCss,
                backgroundColor: tilesColor,
                backfaceVisibility: tilesBackfaceVisibility,
                borderRadius: `${tilesBorderRadius}%`,
                borderWidth: tilesBorderWidth,
                width: tileWidth,
                height: tileHeight,
                left: tileLeft,
                top: tileTop,
                transform: `${tileTransform}`,
                display: 'block',
            })
        }
    }, [interactionPoint, tileBorderRadius, tileHeight, tileLeft, tileTop, tileWidth, tilesBackfaceVisibility, tilesBorderRadius, tilesBorderWidth, tilesColor])

    return <Box id={`interactiveTile${idPostfix}`} ref={tileRef} style={tileCss} />
}

export default InteractiveTile
