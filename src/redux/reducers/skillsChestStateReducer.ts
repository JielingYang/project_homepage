import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type TSkillsChestState = {
  titleToShow: string | undefined,
  textToShow: string,
  logoToShow: string
}

export const initialState: TSkillsChestState = {
  titleToShow: undefined,
  textToShow: "",
  logoToShow: ""
};

export const skillsChestStateSlice = createSlice({
  name: "skillsChestState",
  initialState,
  reducers: {
    _setCardContent: (state: TSkillsChestState, action: PayloadAction<{ title: string | undefined, text: string, logo: string }>) => {
      state.titleToShow = action.payload.title;
      state.textToShow = action.payload.text;
      state.logoToShow = action.payload.logo;
    }
  }
});

export const {
  _setCardContent: setCardContent
} = skillsChestStateSlice.actions;

export default skillsChestStateSlice.reducer;
