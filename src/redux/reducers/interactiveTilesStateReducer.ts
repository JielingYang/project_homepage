import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { TPoint2 } from "../../TYPE";
import { INTERACTIVE_TILE_COLOR_RGBA, TILES_DEFAULT_BORDER_RADIUS, TILES_DEFAULT_BORDER_WIDTH } from "../../interactiveTilesBackground/interactiveTilesBackgroundUtils.ts";
import { Property } from "csstype";

type TInteractiveTilesState = {
  interactionPoint: TPoint2,
  tilesBorderRadius: number,
  tilesBorderWidth: number,
  tilesColor: string,
  tilesBackfaceVisibility: Property.BackfaceVisibility,
}

export const initialState: TInteractiveTilesState = {
  interactionPoint: { x: 0, y: 0 },
  tilesBorderRadius: TILES_DEFAULT_BORDER_RADIUS,
  tilesBorderWidth: TILES_DEFAULT_BORDER_WIDTH,
  tilesColor: INTERACTIVE_TILE_COLOR_RGBA,
  tilesBackfaceVisibility: "hidden",
};

export const interactiveTilesStateSlice = createSlice({
  name: "interactiveTilesState",
  initialState,
  reducers: {
    _setInteractionPoint: (state: TInteractiveTilesState, action: PayloadAction<TPoint2>) => {
      state.interactionPoint = action.payload;
    },
    _setTilesBorderRadius: (state: TInteractiveTilesState, action: PayloadAction<number>) => {
      state.tilesBorderRadius = action.payload;
    },
    _setTilesBorderWidth: (state: TInteractiveTilesState, action: PayloadAction<number>) => {
      state.tilesBorderWidth = action.payload;
    },
    _setTilesColor: (state: TInteractiveTilesState, action: PayloadAction<string>) => {
      state.tilesColor = action.payload;
    },
    _setTilesBackfaceVisible: (state: TInteractiveTilesState, action: PayloadAction<Property.BackfaceVisibility>) => {
      state.tilesBackfaceVisibility = action.payload;
    }
  }
});

export const {
  _setInteractionPoint: setInteractionPoint,
  _setTilesBorderRadius: setTilesBorderRadius,
  _setTilesBorderWidth: setTilesBorderWidth,
  _setTilesColor: setTilesColor,
  _setTilesBackfaceVisible: setTilesBackfaceVisible
} = interactiveTilesStateSlice.actions;

export default interactiveTilesStateSlice.reducer;
