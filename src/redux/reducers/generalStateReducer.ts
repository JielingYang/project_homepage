import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { TPoint3WithCoordLines } from '../../TYPE.ts'

export type GeneralStateType = {
    isPlacingObject: boolean
    pointsWithCoordLines: Array<TPoint3WithCoordLines>
}

export const initialState: GeneralStateType = {
    isPlacingObject: false,
    pointsWithCoordLines: [],
}

export const generalStateSlice = createSlice({
    name: 'generalState',
    initialState,
    reducers: {
        _setIsPlacingObject: (state: GeneralStateType, action: PayloadAction<boolean>) => {
            state.isPlacingObject = action.payload
        },
        _addPointWithCoordLines: (state: GeneralStateType, action: PayloadAction<TPoint3WithCoordLines>) => {
            state.pointsWithCoordLines.push(action.payload)
        },
    },
})

export const { _addPointWithCoordLines: addPointWithCoordLines, _setIsPlacingObject: setIsPlacingObject } = generalStateSlice.actions

export default generalStateSlice.reducer
