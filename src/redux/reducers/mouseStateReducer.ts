import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { TPoint2, TPoint3 } from '../../TYPE.ts'
import { Nullable } from '@babylonjs/core'

export type MouseStateType = {
    mousePositionOnObject: Nullable<TPoint3>
    mouseMovePositionOnWindow: TPoint2
}

export const initialState: MouseStateType = {
    mousePositionOnObject: null,
    mouseMovePositionOnWindow: { x: 0, y: 0 },
}

export const mouseStateSlice = createSlice({
    name: 'mouseState',
    initialState,
    reducers: {
        _setMousePositionOnObject: (state: MouseStateType, action: PayloadAction<TPoint3>) => {
            state.mousePositionOnObject = action.payload
        },
        _setMouseMovePositionOnWindow: (state: MouseStateType, action: PayloadAction<TPoint2>) => {
            state.mouseMovePositionOnWindow = action.payload
        },
    },
})

export const { _setMousePositionOnObject: setMousePositionOnObject, _setMouseMovePositionOnWindow: setMouseMovePositionOnWindow } =
    mouseStateSlice.actions

export default mouseStateSlice.reducer
