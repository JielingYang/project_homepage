import { configureStore } from '@reduxjs/toolkit'
import interactiveTilesStateReducer from './reducers/interactiveTilesStateReducer'
import skillsChestStateReducer from './reducers/skillsChestStateReducer.ts'
import mouseStateReducer from './reducers/mouseStateReducer.ts'
import generalStateReducer from './reducers/generalStateReducer'

export const store = configureStore({
    reducer: {
        mouseState: mouseStateReducer,
        interactiveTilesState: interactiveTilesStateReducer,
        skillsChestState: skillsChestStateReducer,
        generalState: generalStateReducer,
    },
})

export type TRootState = ReturnType<typeof store.getState>
export type TAppDispatch = typeof store.dispatch
export type TAppGetState = typeof store.getState

// export const useAppDispatch = () => useDispatch<AppDispatch>();
// export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
